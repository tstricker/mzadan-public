package org.expasy.mzadan.core.ms;

import com.google.common.base.Preconditions;
import com.google.common.collect.Range;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by Thomas on 11.01.18.
 */
public class SpectrumFiltering {
    private final List<Peak> peakList;
    private final Threshold intensityPercentThreshold;
    private final Threshold intensityCountsThreshold;
    private final Range mzRange;

    public SpectrumFiltering(List<Peak> peakList, Threshold intensityPercentThreshold, Threshold intensityCountsThreshold, Range mzRange) {
        Preconditions.checkNotNull(peakList, "PeakList cannot be null.");
        Preconditions.checkNotNull(intensityPercentThreshold, "IntensityPercentThreshold cannot be null.");
        Preconditions.checkNotNull(intensityCountsThreshold, "IntensityCountsThreshold cannot be null.");
        Preconditions.checkNotNull(mzRange, "MzRange cannot be null.");

        this.peakList = peakList;
        this.intensityPercentThreshold = intensityPercentThreshold;
        this.intensityCountsThreshold = intensityCountsThreshold;
        this.mzRange = mzRange;
    }

    public List<Peak> process() {

        return peakList.stream()
                .filter(peak -> check(peak))
                .collect(Collectors.toList());
    }

    private boolean check(Peak peak) {

        if (!intensityPercentThreshold.check(peak.getRelativeIntensity())) {
            return false;
        }

        if (!intensityCountsThreshold.check(peak.getIntensity())) {
            return false;
        }

        if (!mzRange.contains(peak.getMz())) {
            return false;
        }

        return true;
    }
}
