package org.expasy.mzadan.fx;

import com.jfoenix.controls.JFXDialog;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.expasy.mzadan.core.ms.Annotation;

import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.TreeSet;

/**
 * Created by Thomas on 30.08.18.
 */
public class EditDialogController implements Initializable {
    private JFXDialog jfxDialog;

    @FXML
    private Label titleLabel;

    @FXML
    private TableView<Annotation> activatedAnnotationsTableView;
    @FXML
    private TableView<Annotation> deactivatedAnnotationsTableView;

    @FXML
    private TableColumn<Annotation, String> activatedAnnotationsNameTableColumn;
    @FXML
    private TableColumn<Annotation, Number> activatedAnnotationsValueTableColumn;
    @FXML
    private TableColumn<Annotation, String> deactivatedAnnotationsNameTableColumn;
    @FXML
    private TableColumn<Annotation, Number> deactivatedAnnotationsValueTableColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        activatedAnnotationsNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().getMzShift().nameProperty());
        activatedAnnotationsValueTableColumn.setCellValueFactory(cellData -> cellData.getValue().getMzShift().valueProperty());

        deactivatedAnnotationsNameTableColumn.setCellValueFactory(cellData -> cellData.getValue().getMzShift().nameProperty());
        deactivatedAnnotationsValueTableColumn.setCellValueFactory(cellData -> cellData.getValue().getMzShift().valueProperty());

        activatedAnnotationList.addListener((ListChangeListener<Annotation>)
                c -> activatedAnnotationsTableView.setItems(activatedAnnotationList));

        deactivatedAnnotationList.addListener((ListChangeListener<Annotation>)
                c -> deactivatedAnnotationsTableView.setItems(deactivatedAnnotationList));
    }

    private ObservableSet<Annotation> activatedAnnotationSet;
    private ObservableSet<Annotation> deactivatedAnnotationSet;

    private ObservableList<Annotation> activatedAnnotationList;
    private ObservableList<Annotation> deactivatedAnnotationList;

    public EditDialogController() {
        this.activatedAnnotationList = FXCollections.observableArrayList();
        this.deactivatedAnnotationList = FXCollections.observableArrayList();
    }

    public void setDialog(JFXDialog jfxDialog) {
        this.jfxDialog = jfxDialog;
    }

    public void setTitleLabel(String title) {
        titleLabel.setText(title);
    }

    public void setActivatedAnnotationSet(ObservableSet<Annotation> annotationSet) {
        activatedAnnotationSet = annotationSet;
        activatedAnnotationList.addAll(annotationSet);
    }

    public void setDeactivatedAnnotationSet(ObservableSet<Annotation> annotationSet) {
        deactivatedAnnotationSet = annotationSet;
        deactivatedAnnotationList.addAll(annotationSet);
    }

    @FXML void handleLeftButtonOnAction(){
        if (!activatedAnnotationsTableView.getSelectionModel().isEmpty()) {
            int selectedIndex = activatedAnnotationsTableView.getSelectionModel().getSelectedIndex();
            deactivatedAnnotationList.add(activatedAnnotationList.get(selectedIndex));
            activatedAnnotationList.remove(selectedIndex);
        }
    }

    @FXML void handleRightButtonOnAction(){
        if (!deactivatedAnnotationsTableView.getSelectionModel().isEmpty()) {
            int selectedIndex = deactivatedAnnotationsTableView.getSelectionModel().getSelectedIndex();
            activatedAnnotationList.add(deactivatedAnnotationList.get(selectedIndex));
            deactivatedAnnotationList.remove(selectedIndex);
        }
    }

    @FXML
    public void handleCancelButtonOnAction() {
        jfxDialog.close();
    }

    @FXML
    public void handleOkayButtonOnAction() {
        activatedAnnotationSet.clear();
        activatedAnnotationSet.addAll(activatedAnnotationList);

        deactivatedAnnotationSet.clear();
        deactivatedAnnotationSet.addAll(deactivatedAnnotationList);

        jfxDialog.close();
    }
}
