package org.expasy.mzadan.core.graph.model;

import com.google.common.collect.Range;
import org.expasy.mzadan.core.ms.Peak;
import org.expasy.mzadan.core.ms.Threshold;
import org.expasy.mzadan.core.ms.Tolerance;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;

import java.io.File;
import java.util.Set;

/**
 * Created by Thomas on 10.08.2017.
 */


public class VertexGeneratorTest {
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph0;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph1;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph2;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph3;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph4;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph5;

    @BeforeClass
    public static void setUpClass() throws JMzReaderException {
        jGraph0 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);
        jGraph1 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);
        jGraph2 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);
        jGraph3 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);
        jGraph4 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);
        jGraph5 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);

        File file = new File(VertexGeneratorTest.class.getClassLoader().getResource("VertexGeneratorTestSpectrum.mgf").getFile());
        JMzReader jMzReader = new MgfFile(file);

        GraphGeneratorParams defaultParams = new GraphGeneratorParams();
        defaultParams.setIntensityPercentThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        defaultParams.setIntensityCountsThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        defaultParams.setMzRange(Range.atLeast(0.0));
        defaultParams.setDeisotopingRange(Range.atLeast(0.0));
        defaultParams.setTolerance(new Tolerance(0.005));

        //Testing the conversion and filtering of peaks higher or equal to 0.0% of relative intensity.


        GraphVertexGenerator vertexGenerator0 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator0.process(jMzReader.getSpectrumByIndex(1));

        //Testing the conversion and filtering of peaks higher or equal to 50.0% of relative intensity.
        defaultParams.setIntensityPercentThreshold(new Threshold(50.0, Threshold.Limit.HIGHER));
        defaultParams.setMzRange(Range.atLeast(0.0));

        GraphVertexGenerator vertexGenerator1 = new GraphVertexGenerator(jGraph1, defaultParams);
        vertexGenerator1.process(jMzReader.getSpectrumByIndex(1));

        //Testing the conversion and filtering of peaks higher or equal to 75.0% of relative intensity.
        defaultParams.setIntensityPercentThreshold(new Threshold(75.0, Threshold.Limit.HIGHER));
        defaultParams.setMzRange(Range.atLeast(0.0));

        GraphVertexGenerator vertexGenerator2 = new GraphVertexGenerator(jGraph2, defaultParams);
        vertexGenerator2.process(jMzReader.getSpectrumByIndex(1));

        //Testing the conversion and filtering of peaks higher or equal to 100.0% of relative intensity.
        defaultParams.setIntensityPercentThreshold(new Threshold(100.0, Threshold.Limit.HIGHER));
        defaultParams.setMzRange(Range.atLeast(0.0));

        GraphVertexGenerator vertexGenerator3 = new GraphVertexGenerator(jGraph3, defaultParams);
        vertexGenerator3.process(jMzReader.getSpectrumByIndex(1));

        //Testing the conversion and filtering of peaks higher or equal to 0.0% of relative intensity and ranging from 250.0 to 1000.0 m/z.
        defaultParams.setIntensityPercentThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        defaultParams.setMzRange(Range.closed(250.0, 1000.0));

        GraphVertexGenerator vertexGenerator4 = new GraphVertexGenerator(jGraph4, defaultParams);
        vertexGenerator4.process(jMzReader.getSpectrumByIndex(1));

        //Testing the conversion and filtering of peaks higher or equal to 0.0% of relative intensity and ranging from 500.0 to 750.0.0 m/z.
        defaultParams.setIntensityPercentThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        defaultParams.setMzRange(Range.closed(500.0, 750.0));

        GraphVertexGenerator vertexGenerator5 = new GraphVertexGenerator(jGraph5, defaultParams);
        vertexGenerator5.process(jMzReader.getSpectrumByIndex(1));
    }

    @Test
    public void process() throws Exception {

        Set<Peak> peakSet0 = jGraph0.vertexSet();
        Assert.assertEquals(5, peakSet0.size());

        for (Peak peak : peakSet0) {
            Assert.assertTrue(peak.getRelativeIntensity() >= 0.0);
        }

        Set<Peak> peakSet1 = jGraph1.vertexSet();
        Assert.assertEquals(4, peakSet1.size());

        for (Peak peak : peakSet1) {
            Assert.assertTrue(peak.getRelativeIntensity() >= 50.0);
        }

        Set<Peak> peakSet2 = jGraph2.vertexSet();
        Assert.assertEquals(4, peakSet2.size());

        for (Peak peak : peakSet2) {
            Assert.assertTrue(peak.getRelativeIntensity() >= 75.0);
        }

        Set<Peak> peakSet3 = jGraph3.vertexSet();
        Assert.assertEquals(1, peakSet3.size());

        for (Peak peak : peakSet3) {
            Assert.assertTrue(peak.getRelativeIntensity() >= 100.0);
        }

        Set<Peak> peakSet4 = jGraph4.vertexSet();
        Assert.assertEquals(5, peakSet4.size());

        for (Peak peak : peakSet4) {
            Assert.assertTrue(peak.getMz() >= 250 && peak.getMz() <= 1000.0);
        }

        Set<Peak> peakSet5 = jGraph5.vertexSet();
        Assert.assertEquals(3, peakSet5.size());

        for (Peak peak : peakSet5) {
            Assert.assertTrue(peak.getMz() >= 250.0 && peak.getMz() <= 750.0);
        }
    }
}