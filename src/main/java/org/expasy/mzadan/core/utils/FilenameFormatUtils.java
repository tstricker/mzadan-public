package org.expasy.mzadan.core.utils;


import java.io.File;

/**
 * Created by Thomas on 07.04.17.
 */
public final class FilenameFormatUtils {

    private FilenameFormatUtils() {
    }

    public static String generateOutputNameFromFile(File file, int index, String supplement) {
        return file.getPath().substring(0, file.getPath().indexOf(".")) + "_spectrum" + index + "_" + supplement + ".csv";
    }

    public static String generateOutputNameFromFile(File file, int index) {
        return file.getPath().substring(0, file.getPath().indexOf(".")) + "_spectrum" + index + ".csv";
    }

    public static String generateOutputNameFromFile(File file, String supplement) {
        return file.getPath().substring(0, file.getPath().indexOf(".")) + "_" + supplement + ".csv";
    }
}
