package org.expasy.mzadan.core.ms;

import com.google.common.base.Preconditions;
import com.google.common.collect.Range;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Thomas on 28.06.18.
 */
public class SpectrumDeisotoping {
    private final List<Peak> peakList;
    private final Threshold intensityPercentThreshold;
    private final Range deisotopingRange;
    private final Tolerance tolerance;

    public SpectrumDeisotoping(List<Peak> peakList, Threshold intensityPercentThreshold, Range deisotopingRange, Tolerance tolerance) {
        Preconditions.checkNotNull(peakList, "PeakList cannot be null.");
        Preconditions.checkNotNull(intensityPercentThreshold, "IntensityPercentThreshold cannot be null.");
        Preconditions.checkNotNull(deisotopingRange, "DeisotopingRange cannot be null.");
        Preconditions.checkNotNull(tolerance, "Tolerance cannot be null.");

        this.peakList = peakList;
        this.intensityPercentThreshold = intensityPercentThreshold;
        this.deisotopingRange = deisotopingRange;
        this.tolerance = tolerance;
    }

    public List<Peak> process() {

        peakList.stream()
                .filter(peak -> intensityPercentThreshold.check(peak.getRelativeIntensity()))
                .forEach(peak -> annotateIsotopicPeak(peak));

        return peakList.stream().filter(peak -> peak.getType().equals(Peak.Type.MONOISOTOPIC)).collect(Collectors.toList());
    }

    private void updatePeakAnnotation(Peak basePeak, Peak matchedPeak) {

        if (deisotopingRange.contains(matchedPeak.getRelativeIntensity() / basePeak.getRelativeIntensity() * 100)) {
            if (basePeak.getType().equals(Peak.Type.UNASSIGNED)) {
                basePeak.setType(Peak.Type.MONOISOTOPIC);
            }
            matchedPeak.setType(Peak.Type.ISOTOPIC);
        }
    }

    private void annotateIsotopicPeak(Peak peak) {

        Peak dummyPeak = new Peak(peak.getMz() + 1.00336, 0, 0);

        int matchIndex = Collections.binarySearch(peakList, dummyPeak, new PeakComparatorByMz());

        if (matchIndex < 0.0) {
            matchIndex = (matchIndex + 1) * -1;
        }

        for (int i = matchIndex; i < peakList.size(); i++) {
            if (tolerance.check(dummyPeak.getMz(), peakList.get(i).getMz())) {
                updatePeakAnnotation(peak, peakList.get(i));
            } else {
                break;
            }
        }

        for (int i = matchIndex - 1; i < peakList.size() && i >= 0; i--) {
            if (tolerance.check(dummyPeak.getMz(), peakList.get(i).getMz())) {
                updatePeakAnnotation(peak, peakList.get(i));
            } else {
                break;
            }
        }
    }
}

