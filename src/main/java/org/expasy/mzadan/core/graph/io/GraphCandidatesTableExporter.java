package org.expasy.mzadan.core.graph.io;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.graph.model.AnnotatedWeightedEdge;
import org.expasy.mzadan.core.graph.model.GraphGeneratorParams;
import org.expasy.mzadan.core.graph.scoring.Cluster;
import org.expasy.mzadan.core.graph.scoring.Compound;
import org.expasy.mzadan.core.graph.scoring.GraphScoring;
import org.expasy.mzadan.core.io.CsvFileWriter;
import org.expasy.mzadan.core.ms.Peak;
import org.expasy.mzadan.core.ms.io.SpectrumRecord;
import org.expasy.mzadan.core.utils.NumberFormatUtils;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.io.File;
import java.util.*;

/**
 * Created by Thomas on 26.07.2017.
 */
public class GraphCandidatesTableExporter {
    private final SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph;
    private final GraphGeneratorParams graphGeneratorParams;

    public GraphCandidatesTableExporter(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph, GraphGeneratorParams graphGeneratorParams) {
        Preconditions.checkNotNull(jGraph, "JGraph cannot be null.");
        Preconditions.checkNotNull(graphGeneratorParams, "GraphGeneratorParams cannot be null.");

        this.jGraph = jGraph;
        this.graphGeneratorParams = graphGeneratorParams;
    }

    public void export(File file, SpectrumRecord spectrumRecord) {

        List<List<String>> table = new ArrayList<>();

        GraphScoring graphScoring = new GraphScoring(jGraph);
        List<Cluster> clusterList = graphScoring.process();

        writeHeader(table);

        for (int i = 0; i < clusterList.size(); i++) {
            if (checkCluster(clusterList.get(i))) {
                writeBestScoringCompoundsFromClusters(i, clusterList.get(i), table);
            }
        }

        saveToRecord(table, spectrumRecord);
        CsvFileWriter.writeLines(file, table);
    }

    private boolean checkCluster(Cluster cluster) {

        if (graphGeneratorParams.getClusterConnectivityIndexThreshold() != null) {
            if (!graphGeneratorParams.getClusterConnectivityIndexThreshold().check(cluster.getCompoundList().size())) {
                return false;
            }
        }

        if (graphGeneratorParams.getClusterIntensityCoverageThreshold() != null) {
            if (!graphGeneratorParams.getClusterIntensityCoverageThreshold().check(cluster.getClusterIntensityCoverage())) {
                return false;
            }
        }

        if (graphGeneratorParams.getClusterCountCoverageThreshold() != null) {
            if (!graphGeneratorParams.getClusterCountCoverageThreshold().check(cluster.getClusterCountCoverage())) {
                return false;
            }
        }

        return true;
    }

    private boolean checkCompound(Compound compound) {

        if (graphGeneratorParams.getCompoundPeakIntensityPercentThreshold() != null) {
            if (!graphGeneratorParams.getCompoundPeakIntensityPercentThreshold().check(compound.getPeak().getRelativeIntensity())) {
                return false;
            }
        }

        if (graphGeneratorParams.getCompoundPeakIntensityCountsThreshold() != null) {
            if (!graphGeneratorParams.getCompoundPeakIntensityCountsThreshold().check(compound.getPeak().getIntensity())) {
                return false;
            }
        }

        return true;
    }

    private void writeBestScoringCompoundsFromClusters(int clusterIndex, Cluster cluster, List<List<String>> table) {

        double topScore = cluster.getCompoundList().get(0).getScore();

        for (Compound compound : cluster.getCompoundList()) {
            if (Math.abs(topScore - compound.getScore()) <= 0.001) {
                if (checkCompound(compound)) {
                    table.add(buildRow("C" + Integer.toString(clusterIndex),
                            compound.getPeak(),
                            cluster.getCompoundList().size(),
                            cluster.getClusterIntensityCoverage(),
                            cluster.getClusterCountCoverage())
                    );
                }
            } else {
                break;
            }
        }
    }

    private void writeHeader(List<List<String>> table) {
        List<String> header = new ArrayList<>();
        header.add("id");
        header.add("mass");
        header.add("m/z");
        header.add("intensity");
        header.add("rel. intensity");
        header.add("cci");
        header.add("cic");
        header.add("ccc");

        table.add(header);
    }

    private List<String> buildRow(String id, Peak peak, int cci, double cic, double ccc) {
        List<String> row = new ArrayList<>();

        row.add(id);
        row.add(NumberFormatUtils.roundToString(peak.getMz() - 1.00727, 5));
        row.add(NumberFormatUtils.roundToString(peak.getMz(), 5));
        row.add(NumberFormatUtils.roundToString(peak.getIntensity(), 0));
        row.add(NumberFormatUtils.roundToString(peak.getRelativeIntensity(), 5));
        row.add(Double.toString(cci));
        row.add(NumberFormatUtils.roundToString(cic, 2));
        row.add(NumberFormatUtils.roundToString(ccc, 2));

        return row;
    }


    private void saveToRecord(List<List<String>> table, SpectrumRecord spectrumRecord) {
        table.stream().skip(1).forEach(row -> spectrumRecord.getRecords().add(row));
    }
}
