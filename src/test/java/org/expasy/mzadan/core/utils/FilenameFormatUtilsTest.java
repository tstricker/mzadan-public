package org.expasy.mzadan.core.utils;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Created by Thomas on 09.08.2017.
 */
public class FilenameFormatUtilsTest {
    File file = new File("path\\to\\file\\fileNameFormatUtilsTest.ext");

    @Test
    public void generateOutputNameFromFile() throws Exception {

        Assert.assertEquals(FilenameFormatUtils.generateOutputNameFromFile(file, 1), "path\\to\\file\\fileNameFormatUtilsTest_Spectrum1.csv");
        Assert.assertEquals(FilenameFormatUtils.generateOutputNameFromFile(file, 2), "path\\to\\file\\fileNameFormatUtilsTest_Spectrum2.csv");
        Assert.assertEquals(FilenameFormatUtils.generateOutputNameFromFile(file, 1, "Output"), "path\\to\\file\\fileNameFormatUtilsTest_Spectrum1_Output.csv");
        Assert.assertEquals(FilenameFormatUtils.generateOutputNameFromFile(file, 2, "Output"), "path\\to\\file\\fileNameFormatUtilsTest_Spectrum2_Output.csv");
    }
}