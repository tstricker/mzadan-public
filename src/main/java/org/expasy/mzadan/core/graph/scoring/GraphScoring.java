package org.expasy.mzadan.core.graph.scoring;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.graph.model.AnnotatedWeightedEdge;
import org.expasy.mzadan.core.graph.utils.EdgeUtils;
import org.expasy.mzadan.core.graph.utils.VertexUtils;
import org.expasy.mzadan.core.ms.Peak;

import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.*;


/**
 * Created by Thomas on 27.06.17.
 */
public class GraphScoring {
    private final SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph;

    public GraphScoring(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph) {
        Preconditions.checkNotNull(jGraph, "JGraph cannot be null.");

        this.jGraph = jGraph;
    }

    public List<Cluster> process() {

        ConnectivityInspector connectivityInspector = new ConnectivityInspector(jGraph);
        List<Set<Peak>> connectedPeakSetList = connectivityInspector.connectedSets();

        double spectrumPeakSum = jGraph.vertexSet().size();
        double spectrumIntensitySum = EdgeUtils.sumPeakIntensities(jGraph.vertexSet());

        List<Cluster> clusterList = new ArrayList<>();

        for (Set<Peak> connectedPeakSet : connectedPeakSetList) {

            List<Compound> compoundList = new ArrayList<>();

            connectedPeakSet.stream().forEach(peak -> compoundList.add(
                    new Compound(peak, EdgeUtils.sumPeakIntensities(childrenPeakSetOf(peak)))));

            Collections.sort(compoundList);

            clusterList.add(new Cluster(compoundList,
                    EdgeUtils.sumPeakIntensities(connectedPeakSet) / spectrumIntensitySum * 100,
                    connectedPeakSet.size() / spectrumPeakSum * 100)
            );
        }

        Collections.sort(clusterList);

        return clusterList;
    }

    private Set<Peak> childrenPeakSetOf(Peak peak) {

        Set<Peak> recursionSet = new HashSet<>();
        Set<Peak> outputSet = new HashSet<>();

        recursionSet.add(peak);
        outputSet.add(peak);

        VertexUtils.recursivePeakSetOf(jGraph, recursionSet, outputSet);

        return outputSet;
    }
}
