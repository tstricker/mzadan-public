package org.expasy.mzadan.core.graph.model;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.graph.utils.EdgeUtils;
import org.expasy.mzadan.core.graph.utils.VertexUtils;
import org.expasy.mzadan.core.ms.*;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by Thomas on 06.06.17.
 */
public class GraphEdgeGenerator {
    private final SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph;
    private final GraphGeneratorParams graphGeneratorParams;

    public GraphEdgeGenerator(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph, GraphGeneratorParams graphGeneratorParams) {
        Preconditions.checkNotNull(jGraph, "JGraph cannot be null.");
        Preconditions.checkNotNull(graphGeneratorParams, "GraphGeneratorParams cannot be null.");

        this.jGraph = jGraph;
        this.graphGeneratorParams = graphGeneratorParams;
    }

    public void process() {

        List<Peak> sortedPeakList = VertexUtils.generateSortedPeakList(jGraph);

        boolean neutralAnnotationEnabled = graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.NEUTRAL);
        boolean adductAnnotationEnabled = graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.ADDUCT);
        boolean isotopeAnnotationEnabled = graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.ISOTOPE);
        boolean multimerAnnotationEnbled = graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.MULTIMER);

        for (Peak peak : sortedPeakList) {

            List<AnnotatedWeightedEdge> annotatedWeightedEdges = new ArrayList<>();

            if (neutralAnnotationEnabled) {
                annotatedWeightedEdges.addAll(generateSimpleEdges(peak, graphGeneratorParams.getNeutralAnnotationSet()));
            }

            if (adductAnnotationEnabled) {
                annotatedWeightedEdges.addAll(generateSimpleEdges(peak, graphGeneratorParams.getAdductAnnotationSet()));
            }

            if (isotopeAnnotationEnabled) {
                annotatedWeightedEdges.addAll(generateSimpleEdges(peak, graphGeneratorParams.getIsotopeAnnotationSet()));
            }

            if (multimerAnnotationEnbled) {
                annotatedWeightedEdges.addAll(buildMassMultiplierEdges(peak, graphGeneratorParams.getMassMultiplier().getValue()));
            }

            for (AnnotatedWeightedEdge edge : annotatedWeightedEdges) {
                ConnectVertices(edge, sortedPeakList);
            }
        }
    }

    private void addEdgeAndSetWeight(Peak peak, Peak decoy, AnnotatedWeightedEdge<Peak> edge) {
        jGraph.addEdge(edge.getV1(), edge.getV2(), edge);
        jGraph.setEdgeWeight(edge, EdgeUtils.convertErrorToWeight(graphGeneratorParams.getTolerance(),
                graphGeneratorParams.getTolerance().absoluteError(decoy.getMz(), peak.getMz())));
    }

    private void ConnectVertices(AnnotatedWeightedEdge<Peak> edge, List<Peak> peakList) {

        Peak decoy = new Peak((edge.getV1().getMz() + edge.getAnnotation().getMzShift().getValue()), 0.0, 0.0);

        int matchIndex = Collections.binarySearch(peakList, decoy, new PeakComparatorByMz());

        if (matchIndex < 0.0) {
            matchIndex = (matchIndex + 1) * -1;
        }

        for (int i = matchIndex; i < peakList.size(); i++) {
            if (graphGeneratorParams.getTolerance().check(decoy.getMz(), peakList.get(i).getMz())) {
                addEdgeAndSetWeight(peakList.get(i), decoy, new AnnotatedWeightedEdge<>(edge.getV1(), peakList.get(i), edge.getAnnotation()));
            } else {
                break;
            }
        }

        for (int i = matchIndex - 1; i < peakList.size() && i >= 0; i--) {
            if (graphGeneratorParams.getTolerance().check(decoy.getMz(), peakList.get(i).getMz())) {
                addEdgeAndSetWeight(peakList.get(i), decoy, new AnnotatedWeightedEdge<>(edge.getV1(), peakList.get(i), edge.getAnnotation()));
            } else {
                break;
            }
        }
    }

    private List<AnnotatedWeightedEdge> generateSimpleEdges(Peak peak, Set<Annotation> annotationSet) {

        List<AnnotatedWeightedEdge> annotatedWeightedEdgeList = new ArrayList<>();

        for (Annotation annotation : annotationSet) {
            annotatedWeightedEdgeList.add(new AnnotatedWeightedEdge<>(peak, null, annotation));
        }

        return annotatedWeightedEdgeList;
    }

    private List<AnnotatedWeightedEdge> buildMassMultiplierEdges(Peak peak, int multiplier) {

        List<AnnotatedWeightedEdge> annotatedWeightedEdgeList = new ArrayList<>();

        for (int i = 2; i <= multiplier; i++) {
            MzShift mzShift = new MzShift(i + "M", (peak.getMz() - 1.00728) * (i - 1));
            annotatedWeightedEdgeList.add(new AnnotatedWeightedEdge<>(peak, null, new Annotation(Annotation.Type.MULTIMER, mzShift)));
        }

        return annotatedWeightedEdgeList;
    }
}
