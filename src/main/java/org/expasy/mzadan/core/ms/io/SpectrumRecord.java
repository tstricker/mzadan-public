package org.expasy.mzadan.core.ms.io;

import com.google.common.base.Preconditions;
import uk.ac.ebi.pride.tools.jmzreader.model.Param;
import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thomas on 25.10.17.
 */
public class SpectrumRecord {
    private final String filename;
    private final int index;
    private final String title;
    private final String retentionTime;
    private final List<List<String>> records;

    public SpectrumRecord(String filename, int index, Spectrum spectrum) {
        Preconditions.checkNotNull(filename);
        Preconditions.checkNotNull(spectrum);

        this.filename = filename;
        this.index = index;
        this.title = extractTitleFromParams(spectrum);
        this.retentionTime = extractRetentionTimeFromParams(spectrum);
        records = new ArrayList<>();
    }

    public String getFilename() {
        return filename;
    }

    public int getIndex() {
        return index;
    }

    public String getTitle() {
        return title;
    }

    public List<List<String>> getRecords() {
        return records;
    }

    private String extractTitleFromParams(Spectrum spectrum) {

        for (Param param : spectrum.getAdditional().getParams()) {
            if (param.getName() == "spectrum title") {
                return param.getValue().toString();
            }
        }
        return "";
    }

    private String extractRetentionTimeFromParams(Spectrum spectrum) {

        for (Param param : spectrum.getAdditional().getParams()) {
            if (param.getName() == "retention time") {
                return param.getValue().toString();
            }
        }
        return "";
    }

    @Override
    public String toString() {
        return "SpectrumRecord{" +
                "filename='" + filename + '\'' +
                ", index=" + index +
                ", title='" + title + '\'' +
                ", retentionTime='" + retentionTime + '\'' +
                ", records=" + records +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SpectrumRecord)) return false;

        SpectrumRecord that = (SpectrumRecord) o;

        if (index != that.index) return false;
        if (!filename.equals(that.filename)) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (retentionTime != null ? !retentionTime.equals(that.retentionTime) : that.retentionTime != null)
            return false;
        return records != null ? records.equals(that.records) : that.records == null;
    }

    @Override
    public int hashCode() {
        int result = filename.hashCode();
        result = 31 * result + index;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (retentionTime != null ? retentionTime.hashCode() : 0);
        result = 31 * result + (records != null ? records.hashCode() : 0);
        return result;
    }
}
