package org.expasy.mzadan.core.ms;

import java.util.Comparator;

/**
 * Created by Thomas on 01/11/16.
 */
public class PeakComparatorByMz implements Comparator<Peak> {

    @Override
    public int compare(Peak p1, Peak p2) {
        return Double.compare(p1.getMz(), p2.getMz());
    }
}