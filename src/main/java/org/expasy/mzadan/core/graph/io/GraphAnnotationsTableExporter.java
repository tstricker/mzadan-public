
package org.expasy.mzadan.core.graph.io;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.graph.model.AnnotatedWeightedEdge;
import org.expasy.mzadan.core.graph.model.GraphGeneratorParams;
import org.expasy.mzadan.core.graph.utils.EdgeUtils;
import org.expasy.mzadan.core.io.CsvFileWriter;
import org.expasy.mzadan.core.ms.Annotation;
import org.expasy.mzadan.core.ms.Peak;
import org.expasy.mzadan.core.ms.PeakComparatorByMz;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import java.io.File;
import java.util.*;


/**
 * Created by Thomas on 06.04.17.
 */
public class GraphAnnotationsTableExporter {
    private final SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph;
    private final GraphGeneratorParams graphGeneratorParams;


    public GraphAnnotationsTableExporter(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph, GraphGeneratorParams graphGeneratorParams) {
        Preconditions.checkNotNull(jGraph, "JGraph cannot be null.");
        Preconditions.checkNotNull(graphGeneratorParams, "GraphGeneratorParams cannot be null.");

        this.jGraph = jGraph;
        this.graphGeneratorParams = graphGeneratorParams;
    }

    public void export(File file) {

        List<List<String>> table = new ArrayList<>();
        List<List<String>> headers = buildHeader();

        List<Peak> peakList = new ArrayList<>(jGraph.vertexSet());
        Collections.sort(peakList, new PeakComparatorByMz());

        table.add(headers.get(0));
        table.add(headers.get(1));

        for (Peak peak : peakList) {

            List<String> row = createEmptyRow(table.get(0).size());

            for (AnnotatedWeightedEdge<Peak> edge : jGraph.outgoingEdgesOf(peak)) {

                int position;

                if (edge.getAnnotation().getType() == Annotation.Type.MULTIMER || edge.getAnnotation().getType() == Annotation.Type.MULTICHARGE) {
                    position = table.get(0).indexOf(edge.getAnnotation().getMzShift().getName());
                } else {
                    position = table.get(0).indexOf(edge.getAnnotation().getMzShift().getName() + " (" + edge.getAnnotation().getMzShift().getValue() + ")");
                }
                row.set(position++, Double.toString(edge.getV2().getMz()));
                row.set(position++, Double.toString(edge.getV2().getIntensity()));
                row.set(position++, Double.toString(edge.getV2().getRelativeIntensity()));
                row.set(position++, Double.toString(EdgeUtils.convertWeightToError(graphGeneratorParams.getTolerance(), jGraph.getEdgeWeight(edge))));
            }
            row.set(0, Double.toString(peak.getMz()));
            row.set(1, Double.toString(peak.getIntensity()));
            row.set(2, Double.toString(peak.getRelativeIntensity()));

            table.add(row);
        }

        CsvFileWriter.writeLines(file, table);
    }

    private void addSpacer(List<String> list, int spacerCount, String spacer) {
        for (int i = 0; i < spacerCount; i++) {
            list.add(spacer);
        }
    }

    private void addSpacer(List<String> list, int spacerPosition, int spacerCount, String spacer) {
        for (int i = 0; i < spacerCount; i++) {
            list.add(spacerPosition, spacer);
        }
    }

    private List<String> createEmptyRow(int columns) {

        List<String> annotationList = new ArrayList<>(columns);

        for (int i = 0; i < columns; i++) {
            annotationList.add("0.0");
        }

        return annotationList;
    }

    private List<List<String>> buildHeader() {

        List<String> mainHeader = new ArrayList<>();
        List<String> subHeader = new ArrayList<>();

        List<String> subHeaderContent = new ArrayList<>();
        subHeaderContent.add("m/z");
        subHeaderContent.add("intensity (cps)");
        subHeaderContent.add("intensity (%)");
        subHeaderContent.add("error (u)");

        subHeader.addAll(subHeaderContent.subList(0, 3));

        if (graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.NEUTRAL)) {
            for (Annotation annotation : graphGeneratorParams.getNeutralAnnotationSet()) {
                mainHeader.add(annotation.getMzShift().getName() + " (" + annotation.getMzShift().getValue() + ")");
                addSpacer(mainHeader, 3, "");

                subHeader.addAll(subHeaderContent);
            }
        }

        if (graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.ADDUCT)) {
            for (Annotation annotation : graphGeneratorParams.getAdductAnnotationSet()) {
                mainHeader.add(annotation.getMzShift().getName() + " (" + annotation.getMzShift().getValue() + ")");
                addSpacer(mainHeader, 3, "");

                subHeader.addAll(subHeaderContent);
            }
        }

        if (graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.ISOTOPE)) {
            for (Annotation annotation : graphGeneratorParams.getIsotopeAnnotationSet()) {
                mainHeader.add(annotation.getMzShift().getName() + " (" + annotation.getMzShift().getValue() + ")");
                addSpacer(mainHeader, 3, "");

                subHeader.addAll(subHeaderContent);
            }
        }

        if (graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.MULTIMER)) {
            for (int i = 2; i <= graphGeneratorParams.getMassMultiplier().getValue(); i++) {
                mainHeader.add(i + "M");
                addSpacer(mainHeader, 3, "");

                subHeader.addAll(subHeaderContent);
            }
        }

        if (graphGeneratorParams.getAnnotationTypeSet().contains(Annotation.Type.MULTICHARGE)) {
            for (int i = 1; i <= graphGeneratorParams.getChargeMultiplier().getValue(); i++) {
                mainHeader.add(i + "C");
                addSpacer(mainHeader, 2, "");

                subHeader.addAll(subHeaderContent);
            }
        }

        addSpacer(mainHeader, 0, 3, "");

        List<List<String>> table = new ArrayList();
        table.add(mainHeader);
        table.add(subHeader);

        return table;
    }
}


