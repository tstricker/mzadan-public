package org.expasy.mzadan.core.graph.utils;

import org.expasy.mzadan.core.graph.model.AnnotatedWeightedEdge;
import org.expasy.mzadan.core.ms.Peak;
import org.expasy.mzadan.core.ms.PeakComparatorByMz;
import org.expasy.mzadan.core.ms.Threshold;
import org.expasy.mzadan.core.utils.NumberFormatUtils;
import org.jgrapht.Graphs;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;

import java.util.*;

/**
 * Created by Thomas on 22.06.17.
 */
public final class VertexUtils {

    private VertexUtils() {
    }

    public static List<Peak> generateSortedPeakList(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph) {

        List<Peak> peakList = new ArrayList(jGraph.vertexSet());
        Collections.sort(peakList, new PeakComparatorByMz());

        return peakList;
    }

    public static List<Peak> generateSortedPeakList(Spectrum spectrum) {

        List<Peak> peakList = new ArrayList<>();

        double maxIntensity = Collections.max(spectrum.getPeakList().values());

        for (Map.Entry<Double, Double> jMzPeak : spectrum.getPeakList().entrySet()) {

            double mz = jMzPeak.getKey();
            double intensity = jMzPeak.getValue();
            double relativeIntensity = jMzPeak.getValue() / maxIntensity * 100;

            peakList.add(new Peak(NumberFormatUtils.round(mz, 5),
                    NumberFormatUtils.round(intensity, 2),
                    NumberFormatUtils.round(relativeIntensity, 2)));
        }

        peakList.sort(new PeakComparatorByMz());
        return peakList;
    }

    public static List<Peak> generateSortedPeakList(Spectrum spectrum, Threshold threshold) {

        List<Peak> peakList = new ArrayList<>();

        double maxIntensity = Collections.max(spectrum.getPeakList().values());

        for (Map.Entry<Double, Double> jMzPeak : spectrum.getPeakList().entrySet()) {

            double mz = jMzPeak.getKey();
            double intensity = jMzPeak.getValue();
            double relativeIntensity = jMzPeak.getValue() / maxIntensity * 100;

            if (threshold.check(relativeIntensity)) {
                peakList.add(new Peak(NumberFormatUtils.round(mz, 5),
                        NumberFormatUtils.round(intensity, 2),
                        NumberFormatUtils.round(relativeIntensity, 2)));
            }
        }

        peakList.sort(new PeakComparatorByMz());
        return peakList;
    }

    public static void recursivePeakSetOf(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph, Set<Peak> recursionSet, Set<Peak> outputSet) {

        Set<Peak> successorSet = new HashSet();

        for (Peak peak : recursionSet) {
            for (Peak recursionPeak : Graphs.successorListOf(jGraph, peak)) {
                if (!outputSet.contains(recursionPeak)) {
                    successorSet.add(recursionPeak);
                }
            }
        }

        if (!successorSet.isEmpty()) {
            outputSet.addAll(successorSet);
            recursivePeakSetOf(jGraph, successorSet, outputSet);
        } else {
            return;
        }
    }
}
