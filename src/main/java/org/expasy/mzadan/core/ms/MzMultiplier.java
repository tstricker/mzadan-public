package org.expasy.mzadan.core.ms;

import com.google.common.base.Preconditions;

/**
 * Created by Thomas on 12.01.18.
 */
public class MzMultiplier implements Comparable<MzMultiplier> {
    private final int value;
    private final Type type;


    public enum Type {
        MASS, CHARGE
    }

    public MzMultiplier(MzMultiplier mzMultiplier) {
        this(mzMultiplier.getValue(), mzMultiplier.getType());
    }

    public MzMultiplier(int value, Type type) {
        Preconditions.checkNotNull(type, "Type cannot be null.");

        this.value = value;
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "MzMultiplier{" +
                "value=" + value +
                ", type=" + type +
                '}';
    }

    @Override
    public int compareTo(MzMultiplier o) {
        int value = this.getType().toString().compareTo(o.getType().toString());
        return value == 0 ? Double.compare(this.value, o.getValue()) : value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MzMultiplier)) return false;

        MzMultiplier that = (MzMultiplier) o;

        if (value != that.value) return false;
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = value;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
