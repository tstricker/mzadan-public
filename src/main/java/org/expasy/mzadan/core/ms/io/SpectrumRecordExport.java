package org.expasy.mzadan.core.ms.io;

import org.expasy.mzadan.core.io.CsvFileWriter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Thomas on 25.10.17.
 */
public class SpectrumRecordExport {
    private final static Pattern rangePattern = Pattern.compile("([0-9]+\\.[0-9]{1,5}) to ([0-9]+\\.[0-9]{1,5})");
    private final static Pattern discretePattern = Pattern.compile("[0-9]+\\.[0-9]+");

    public SpectrumRecordExport() {

    }

    public void export(File file, List<SpectrumRecord> spectrumRecordList) {

        List<List<String>> table = new ArrayList<>();

        table.add(buildHeader());

        for (SpectrumRecord spectrumRecord : spectrumRecordList) {
            String retentionTime = extractRetentionTime(spectrumRecord.getTitle());

            for (List<String> record : spectrumRecord.getRecords()) {
                List<String> row = new ArrayList<>();
                row.add(spectrumRecord.getFilename());
                row.add(retentionTime);
                row.add(Integer.toString(spectrumRecord.getIndex()));
                row.addAll(record);

                table.add(row);
            }
        }
        CsvFileWriter.writeLines(file, table);
    }

    private String extractRetentionTime(String title) {

        Matcher rangeMatcher = rangePattern.matcher(title);
        Matcher discreteMatcher = discretePattern.matcher(title);

        if (rangeMatcher.find()) {
            return rangeMatcher.group(1) + "-" + rangeMatcher.group(2);
        } else if (discreteMatcher.find()){
            return discreteMatcher.group(0);
        }

        return "NA";
    }

    private List<String> buildHeader() {
        List<String> header = new ArrayList<>();
        header.add("file name");
        header.add("rt");
        header.add("index");
        header.add("id");
        header.add("mass");
        header.add("m/z");
        header.add("intensity");
        header.add("rel. intensity");
        header.add("cci");
        header.add("cic");
        header.add("ccc");

        return header;
    }
}
