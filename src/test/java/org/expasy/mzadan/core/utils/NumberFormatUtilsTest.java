package org.expasy.mzadan.core.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Thomas on 09.08.1017.
 */
public class NumberFormatUtilsTest {
    @Test
    public void round() throws Exception {

        Assert.assertEquals(0.1, NumberFormatUtils.round(0.09, 1), 0);
        Assert.assertEquals(-0.1, NumberFormatUtils.round(-0.09, 1), 0);
        Assert.assertEquals(1.1, NumberFormatUtils.round(1.09, 1), 0);
        Assert.assertEquals(-1.1, NumberFormatUtils.round(-1.09, 1), 0);
        Assert.assertEquals(0.01, NumberFormatUtils.round(0.009, 2), 0);
        Assert.assertEquals(-0.01, NumberFormatUtils.round(-0.009, 2), 0);
        Assert.assertEquals(1.01, NumberFormatUtils.round(1.009, 2), 0);
        Assert.assertEquals(-1.01, NumberFormatUtils.round(-1.009, 2), 0);
        Assert.assertEquals(0.001, NumberFormatUtils.round(0.0009, 3), 0);
        Assert.assertEquals(-0.001, NumberFormatUtils.round(-0.0009, 3), 0);
        Assert.assertEquals(1.001, NumberFormatUtils.round(1.0009, 3), 0);
        Assert.assertEquals(-1.001, NumberFormatUtils.round(-1.0009, 3), 0);
    }

    @Test
    public void roundToString() throws Exception {

        Assert.assertEquals("0.1", NumberFormatUtils.roundToString(0.09, 1));
        Assert.assertEquals("-0.1", NumberFormatUtils.roundToString(-0.09, 1));
        Assert.assertEquals("1.1", NumberFormatUtils.roundToString(1.09, 1));
        Assert.assertEquals("-1.1", NumberFormatUtils.roundToString(-1.09, 1));
        Assert.assertEquals("0.01", NumberFormatUtils.roundToString(0.009, 2));
        Assert.assertEquals("-0.01", NumberFormatUtils.roundToString(-0.009, 2));
        Assert.assertEquals("1.01", NumberFormatUtils.roundToString(1.009, 2));
        Assert.assertEquals("-1.01", NumberFormatUtils.roundToString(-1.009, 2));
        Assert.assertEquals("0.001", NumberFormatUtils.roundToString(0.0009, 3));
        Assert.assertEquals("-0.001", NumberFormatUtils.roundToString(-0.0009, 3));
        Assert.assertEquals("1.001", NumberFormatUtils.roundToString(1.0009, 3));
        Assert.assertEquals("-1.001", NumberFormatUtils.roundToString(-1.0009, 3));
    }
}