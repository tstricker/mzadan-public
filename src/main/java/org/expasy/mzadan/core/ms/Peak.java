package org.expasy.mzadan.core.ms;

import com.google.common.base.Preconditions;


/**
 * Created by Thomas on 06.06.17.
 */
public class Peak implements Comparable<Peak> {
    private final double mz;
    private final double intensity;
    private final double relativeIntensity;

    private Type type;

    public enum Type {
        UNASSIGNED, MONOISOTOPIC, ISOTOPIC
    }

    public Peak(Peak peak) {
        this(peak.getMz(), peak.getIntensity(), peak.getRelativeIntensity(), peak.getType());
    }

    public Peak(double mz, double intensity, double relativeIntensity) {
        this(mz, intensity, relativeIntensity, Type.UNASSIGNED);
    }

    public Peak(double mz, double intensity, double relativeIntensity, Type type) {
        Preconditions.checkArgument(mz >= 0.0, "Mz cannot be negative: %d ", mz);
        Preconditions.checkArgument(intensity >= 0.0, "Intensity cannot be negative: %d ", intensity);
        Preconditions.checkArgument(relativeIntensity >= 0.0 && relativeIntensity <= 100.0, "Relative Intensity must be contained between 0 and 100.0: %d ", relativeIntensity);

        this.mz = mz;
        this.intensity = intensity;
        this.relativeIntensity = relativeIntensity;

        this.type = type;
    }

    public double getMz() {
        return mz;
    }

    public double getIntensity() {
        return intensity;
    }

    public double getRelativeIntensity() {
        return relativeIntensity;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Peak{" +
                "mz=" + mz +
                ", intensity=" + intensity +
                ", relativeIntensity=" + relativeIntensity +
                ", type=" + type +
                '}';
    }

    @Override
    public int compareTo(Peak o) {
        int mz = Double.compare(this.mz, o.getMz());
        return mz == 0 ? Double.compare(this.intensity, o.getIntensity()) : mz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Peak)) return false;

        Peak peak = (Peak) o;

        if (Double.compare(peak.mz, mz) != 0) return false;
        if (Double.compare(peak.intensity, intensity) != 0) return false;
        if (Double.compare(peak.relativeIntensity, relativeIntensity) != 0) return false;
        return type == peak.type;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(mz);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(intensity);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(relativeIntensity);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + type.hashCode();
        return result;
    }
}
