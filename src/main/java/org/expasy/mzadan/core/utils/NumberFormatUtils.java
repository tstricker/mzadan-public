package org.expasy.mzadan.core.utils;

/**
 * Created by Thomas Stricker on 21/03/2017.
 */
public final class NumberFormatUtils {

    private NumberFormatUtils() {
    }

    public static double round(double value, int decimals) {
        return Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
    }

    public static String roundToString(double value, int decimals){
        return Double.toString(Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals));
    }
}
