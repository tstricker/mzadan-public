package org.expasy.mzadan.core.ms;

import com.google.common.base.Preconditions;

/**
 * Created by Thomas on 06.06.17.
 */
public class Tolerance {
    private final double value;
    private static final double SCALE = 1000000;

    public Tolerance (Tolerance tolerance) {
        this(tolerance.getValue());
    }

    public Tolerance(double value) {
        Preconditions.checkArgument(value >= 0.0, "Value cannot be negative: %d ", value);

        this.value = value;
    }

    public boolean check(double actual, double expected) {
        return Math.abs((int) (actual * SCALE) - (int) (expected * SCALE)) <= (int) (value * SCALE);
    }

    public double absoluteError(double actual, double expected) {
        return Math.abs(actual - expected);
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Tolerance{" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tolerance tolerance = (Tolerance) o;

        return Double.compare(tolerance.value, value) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(value);
        return (int) (temp ^ (temp >>> 32));
    }
}