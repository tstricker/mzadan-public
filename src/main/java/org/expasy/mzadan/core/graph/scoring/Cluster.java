package org.expasy.mzadan.core.graph.scoring;

import com.google.common.base.Preconditions;

import java.util.List;

/**
 * Created by Thomas on 08.03.18.
 */
public class Cluster implements Comparable<Cluster> {
    private final List<Compound> compoundList;
    private final double clusterIntensityCoverage;
    private final double clusterCountCoverage;

    public Cluster(Cluster cluster) {
        this(cluster.compoundList, cluster.getClusterIntensityCoverage(), cluster.getClusterCountCoverage());
    }

    public Cluster(List<Compound> compoundList, double clusterIntensityCoverage, double clusterCountCoverage) {
        Preconditions.checkNotNull(compoundList, "Compound list cannot be null.");

        this.compoundList = compoundList;
        this.clusterIntensityCoverage = clusterIntensityCoverage;
        this.clusterCountCoverage = clusterCountCoverage;
    }

    public List<Compound> getCompoundList() {
        return compoundList;
    }

    public double getClusterIntensityCoverage() {
        return clusterIntensityCoverage;
    }


    public double getClusterCountCoverage() {
        return clusterCountCoverage;
    }

    @Override
    public String toString() {
        return "Cluster{" +
                "compoundList=" + compoundList +
                ", clusterIntensityCoverage=" + clusterIntensityCoverage +
                ", clusterCountCoverage=" + clusterCountCoverage +
                '}';
    }

    public int compareTo(Cluster c) {
        return Double.compare(c.getCompoundList().get(0).getPeak().getIntensity(), this.compoundList.get(0).getPeak().getIntensity());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cluster)) return false;

        Cluster cluster = (Cluster) o;

        if (Double.compare(cluster.clusterIntensityCoverage, clusterIntensityCoverage) != 0) return false;
        if (Double.compare(cluster.clusterCountCoverage, clusterCountCoverage) != 0) return false;
        return compoundList.equals(cluster.compoundList);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = compoundList.hashCode();
        temp = Double.doubleToLongBits(clusterIntensityCoverage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(clusterCountCoverage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
