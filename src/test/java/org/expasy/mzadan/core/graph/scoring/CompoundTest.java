package org.expasy.mzadan.core.graph.scoring;

import org.expasy.mzadan.core.ms.Peak;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Thomas on 10.08.2017.
 */
public class CompoundTest {
    @Test
    public void compareTo() throws Exception {
        Compound compound0 = new Compound(new Peak(500.0, 10000.0, 100.0), 100.0);
        Compound compound1 = new Compound(new Peak(500.0, 10000.0, 100.0), 50.0);
        Compound compound2 = new Compound(new Peak(500.0, 10000.0, 100.0), 25.0);
        Compound compound3 = new Compound(new Peak(500.0, 10000.0, 100.0), 12.5);

        Assert.assertEquals(-1, compound0.compareTo(compound1));
        Assert.assertEquals(0, compound0.compareTo(new Compound(compound0)));
        Assert.assertEquals(1, compound1.compareTo(compound0));
        Assert.assertEquals(-1, compound1.compareTo(compound2));
        Assert.assertEquals(0, compound1.compareTo(new Compound(compound1)));
        Assert.assertEquals(1, compound2.compareTo(compound1));
        Assert.assertEquals(-1, compound2.compareTo(compound3));
        Assert.assertEquals(0, compound2.compareTo(new Compound(compound2)));
    }

    @Test(expected = NullPointerException.class)
    public void testNullPointerExceptionPeak() {
        new Compound(null, 0.0);
    }
}