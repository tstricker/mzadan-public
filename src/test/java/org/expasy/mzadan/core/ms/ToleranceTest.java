package org.expasy.mzadan.core.ms;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Thomas on 09.08.2017.
 */
public class ToleranceTest {
    @Test
    public void check() throws Exception {

        Assert.assertTrue(new Tolerance(1.0).check(0, 1.0));
        Assert.assertTrue(new Tolerance(1.0).check(0, 0.9));
        Assert.assertTrue(new Tolerance(1.0).check(-0.5, 0.5));
        Assert.assertTrue(new Tolerance(1.0).check(0.5, -0.5));
        Assert.assertTrue(new Tolerance(1.0).check(0, -0.9));
        Assert.assertTrue(new Tolerance(1.0).check(0, -1.0));
        Assert.assertTrue(new Tolerance(1.0).check(-1, -1.0));
        Assert.assertTrue(new Tolerance(1.0).check(0, 0));
        Assert.assertTrue(new Tolerance(1.0).check(1, 1.0));
        Assert.assertFalse(new Tolerance(1.0).check(0, 1.1));
        Assert.assertFalse(new Tolerance(0.5).check(0, 1.0));
        Assert.assertFalse(new Tolerance(0.5).check(-1.0, 0));
        Assert.assertFalse(new Tolerance(1.0).check(-1.0, 1.0));
        Assert.assertFalse(new Tolerance(1.0).check(1.0, -1.0));
        Assert.assertFalse(new Tolerance(1.0).check(-1.0, 1.0));
    }

    @Test
    public void absoluteError() throws Exception {

        Assert.assertEquals(0, new Tolerance(0).absoluteError(-1, -1), 0);
        Assert.assertEquals(0, new Tolerance(0).absoluteError(0, 0), 0);
        Assert.assertEquals(0, new Tolerance(0).absoluteError(1, 1), 0);
        Assert.assertEquals(1.0, new Tolerance(1.0).absoluteError(0, -1), 0);
        Assert.assertEquals(1.0, new Tolerance(1.0).absoluteError(0.5, -0.5), 0);
        Assert.assertEquals(1.0, new Tolerance(1.0).absoluteError(-0.5, 0.5), 0);
        Assert.assertEquals(1.0, new Tolerance(1.0).absoluteError(-1, 0), 0);
    }
}