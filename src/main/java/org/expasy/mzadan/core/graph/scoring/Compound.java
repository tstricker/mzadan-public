package org.expasy.mzadan.core.graph.scoring;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.ms.Peak;

/**
 * Created by Thomas on 27.07.2017.
 */
public class Compound implements Comparable<Compound> {
    private final Peak peak;
    private double score;

    public Compound(Compound compound){
        this(compound.getPeak(), compound.getScore());
    }

    public Compound(Peak peak) {
        this(peak, -1.0);
    }

    public Compound(Peak peak, double score) {
        Preconditions.checkNotNull(peak, "Peak cannot be null.");

        this.peak = peak;
        this.score = score;
    }

    public Peak getPeak() {
        return peak;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Compound{" +
                "peak=" + peak +
                ", score=" + score +
                '}';
    }

    @Override
    public int compareTo(Compound c) {
        return Double.compare(c.getScore(), this.score);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Compound compound = (Compound) o;

        if (Double.compare(compound.score, score) != 0) return false;
        return peak.equals(compound.peak);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = peak.hashCode();
        temp = Double.doubleToLongBits(score);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
