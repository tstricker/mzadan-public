package org.expasy.mzadan.core.graph.scoring;


import com.google.common.collect.Range;
import org.expasy.mzadan.core.graph.model.*;
import org.expasy.mzadan.core.ms.*;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Thomas on 26.07.18.
 */
public class GraphScoringTest {
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph0;

    @BeforeClass
    public static void setUpClass() throws JMzReaderException {
        File file = new File(VertexGeneratorTest.class.getClassLoader().getResource("GraphScoringTestSpectrum.mgf").getFile());
        JMzReader jMzReader = new MgfFile(file);

        jGraph0 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);

        Set<Annotation.Type> annotationTypeSet = new HashSet();
        annotationTypeSet.add(Annotation.Type.NEUTRAL);
        annotationTypeSet.add(Annotation.Type.ADDUCT);
        annotationTypeSet.add(Annotation.Type.ISOTOPE);
        annotationTypeSet.add(Annotation.Type.MULTIMER);

        GraphGeneratorParams graphGeneratorParams = new GraphGeneratorParams();
        graphGeneratorParams.setIntensityPercentThreshold(new Threshold(1.0, Threshold.Limit.HIGHER));
        graphGeneratorParams.setIntensityCountsThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        graphGeneratorParams.setMzRange(Range.atLeast(0.0));
        graphGeneratorParams.setDeisotopingRange(Range.closed(0.0, 100.0));
        graphGeneratorParams.setAnnotationTypeSet(annotationTypeSet);
        graphGeneratorParams.setAdductAnnotationSet(AnnotationSingleton.INSTANCE.getAdductDefaultAnnotationSet());
        graphGeneratorParams.setNeutralAnnotationSet(AnnotationSingleton.INSTANCE.getNeutralDefaultAnnotationSet());
        graphGeneratorParams.setIsotopeAnnotationSet(AnnotationSingleton.INSTANCE.getIsotopeDefaultAnnotationSet());
        graphGeneratorParams.setMassMultiplier(new MzMultiplier(2, MzMultiplier.Type.MASS));
        graphGeneratorParams.setTolerance(new Tolerance(0.005));

        GraphVertexGenerator vertexGenerator = new GraphVertexGenerator(jGraph0, graphGeneratorParams);
        vertexGenerator.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator = new GraphEdgeGenerator(jGraph0, graphGeneratorParams);
        edgeGenerator.process();
    }

    @Test
    public void process() {

        GraphScoring compoundScoring = new GraphScoring(jGraph0);
        List<Cluster> clusterList = compoundScoring.process();

        Assert.assertEquals(new Peak(217.02655, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), clusterList.get(0).getCompoundList().get(0).getPeak());
        Assert.assertEquals(new Peak(200.0, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), clusterList.get(0).getCompoundList().get(1).getPeak());
        Assert.assertEquals(new Peak(517.02655, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), clusterList.get(1).getCompoundList().get(0).getPeak());
        Assert.assertEquals(new Peak(500.0, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), clusterList.get(1).getCompoundList().get(1).getPeak());
    }
}
