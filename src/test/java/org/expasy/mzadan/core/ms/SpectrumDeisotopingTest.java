package org.expasy.mzadan.core.ms;


import com.google.common.collect.Range;
import org.expasy.mzadan.core.graph.model.GraphGeneratorParams;
import org.expasy.mzadan.core.graph.utils.VertexUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;

import java.io.File;
import java.util.List;

/**
 * Created by Thomas on 23.07.18.
 */

public class SpectrumDeisotopingTest {
    private static List<Peak> defaultPeakList0;
    private static List<Peak> defaultPeakList1;

    @BeforeClass
    public static void setUpClass() throws JMzReaderException {
        File file0 = new File(SpectrumDeisotoping.class.getClassLoader().getResource("VertexGeneratorTestSpectrum.mgf").getFile());
        File file1 = new File(SpectrumDeisotoping.class.getClassLoader().getResource("EdgeGeneratorTestSpectrum.mgf").getFile());

        JMzReader jMzReader0 = new MgfFile(file0);
        JMzReader jMzReader2 = new MgfFile(file1);

        Spectrum spectrum1 = jMzReader0.getSpectrumByIndex(1);
        Spectrum spectrum2 = jMzReader2.getSpectrumByIndex(1);

        defaultPeakList0 = VertexUtils.generateSortedPeakList(spectrum1, new Threshold(0.0, Threshold.Limit.HIGHER));
        defaultPeakList1 = VertexUtils.generateSortedPeakList(spectrum2, new Threshold(0.0, Threshold.Limit.HIGHER));
    }

    @Test
    public void SpectrumDeisotopingTest() {

        //Testing the conversion and deisotoping of peaks higher than 0% of relative intensity and deisotoping intensity range of 0.0 to 100.0.
        SpectrumDeisotoping spectrumDeisotoping0 = new SpectrumDeisotoping(defaultPeakList0,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.closed(0.0, 100.0),
                new Tolerance(0.005)
        );

        List<Peak> peakList0 = spectrumDeisotoping0.process();

        Assert.assertEquals(new Peak(482.97345, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), peakList0.get(0));
        Assert.assertEquals(new Peak(500.0, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), peakList0.get(1));
        Assert.assertEquals(new Peak(517.02655, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), peakList0.get(2));
        Assert.assertEquals(new Peak(521.98195, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), peakList0.get(3));
        Assert.assertEquals(new Peak(1000.0, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), peakList0.get(4));

        //Testing the conversion and deisotoping of peaks higher than 0% of relative intensity and deisotoping intensity range of 0.0 to 100.0.
        SpectrumDeisotoping spectrumDeisotoping1 = new SpectrumDeisotoping(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.closed(0.0, 100.0),
                new Tolerance(0.005)
        );

        List<Peak> peakList1 = spectrumDeisotoping1.process();

        Assert.assertEquals(new Peak(231.98944, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(0));
        Assert.assertEquals(new Peak(250.0, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), peakList1.get(1));
        Assert.assertEquals(new Peak(271.98195, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(2));
        Assert.assertEquals(new Peak(280.0, 100.0, 1.0, Peak.Type.MONOISOTOPIC), peakList1.get(3));
        Assert.assertEquals(new Peak(453.99453, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), peakList1.get(4));
        Assert.assertEquals(new Peak(472.00509, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(5));
        Assert.assertEquals(new Peak(480.0, 100.0, 1.0, Peak.Type.MONOISOTOPIC), peakList1.get(6));
        Assert.assertEquals(new Peak(481.98944, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(7));
        Assert.assertEquals(new Peak(482.97345, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(8));
        Assert.assertEquals(new Peak(490.0, 100.0, 1.0, Peak.Type.MONOISOTOPIC), peakList1.get(9));
        Assert.assertEquals(new Peak(500.0, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), peakList1.get(10));
        Assert.assertEquals(new Peak(517.02655, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(11));
        Assert.assertEquals(new Peak(520.0, 100.0, 1.0, Peak.Type.MONOISOTOPIC), peakList1.get(12));
        Assert.assertEquals(new Peak(521.98195, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(13));
        Assert.assertEquals(new Peak(534.0531, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), peakList1.get(14));
        Assert.assertEquals(new Peak(537.95589, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(15));
        Assert.assertEquals(new Peak(540.0, 100.0, 1.0, Peak.Type.MONOISOTOPIC), peakList1.get(16));
        Assert.assertEquals(new Peak(543.9639, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), peakList1.get(17));
        Assert.assertEquals(new Peak(998.99272, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(18));
    }
}
