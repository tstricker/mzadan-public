package org.expasy.mzadan;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.expasy.mzadan.fx.MainStageController;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Thomas on 06.06.17.
 */
public class MainFx extends Application {
    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(MainFx.class.getClassLoader().getResource("org/expasy/mzadan/fx/fxml/mainStage.fxml"));
            Parent root = (BorderPane) fxmlLoader.load();

            primaryStage.setTitle("MzAdan");
            primaryStage.setResizable(false);
            primaryStage.setOnCloseRequest(event -> Platform.exit());

            MainStageController mainController = fxmlLoader.getController();
            mainController.setMainFx(this);

            primaryStage.setScene(new Scene(root));
            primaryStage.show();

        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "An error occured in Launcher", ex);
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}