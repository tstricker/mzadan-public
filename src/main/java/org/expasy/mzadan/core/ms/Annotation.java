package org.expasy.mzadan.core.ms;


import com.google.common.base.Preconditions;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Created by Thomas on 06.06.17.
 */
public class Annotation extends RecursiveTreeObject<Annotation> implements Comparable<Annotation> {
    private final ObjectProperty<Type> type;
    private final ObjectProperty<MzShift> mzShift;

    public enum Type {
        NEUTRAL, ADDUCT, ISOTOPE, MULTIMER, MULTICHARGE
    }

    public Annotation(Annotation annotation) {
        this(annotation.getType(), annotation.getMzShift());
    }

    public Annotation(Type type, MzShift mzShift) {
        Preconditions.checkNotNull(mzShift, "MzShift cannot be null");
        Preconditions.checkNotNull(type, "Type cannot be null");

        this.type = new SimpleObjectProperty<>(type);
        this.mzShift = new SimpleObjectProperty<>(mzShift);
    }

    public Type getType() {
        return type.get();
    }

    public ObjectProperty<Type> typeProperty() {
        return type;
    }

    public void setType(Type type) {
        this.type.set(type);
    }

    public MzShift getMzShift() {
        return mzShift.get();
    }

    public ObjectProperty<MzShift> mzShiftProperty() {
        return mzShift;
    }

    public void setMzShift(MzShift mzShift) {
        this.mzShift.set(mzShift);
    }

    public int compareTo(Annotation o) {
        return this.mzShift.get().compareTo(o.getMzShift());
    }

    @Override
    public String toString() {
        return "Annotation{" +
                "type=" + type +
                ", mzShift=" + mzShift +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Annotation)) return false;

        Annotation that = (Annotation) o;

        if (!type.equals(that.type)) return false;
        return mzShift.equals(that.mzShift);
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + mzShift.hashCode();
        return result;
    }
}
