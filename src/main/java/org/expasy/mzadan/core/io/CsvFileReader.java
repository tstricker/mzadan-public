package org.expasy.mzadan.core.io;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Thomas on 12.04.17.
 */
public class CsvFileReader {

    private CsvFileReader() {
    }

    public static List<Map> read(File file) throws IOException {

        Reader fileReader = null;
        CSVParser csvFileParser = null;

        CSVFormat csvFileFormat = CSVFormat.RFC4180.withFirstRecordAsHeader();

        List<Map> lines = new ArrayList();

        fileReader = new FileReader(file);
        csvFileParser = new CSVParser(fileReader, csvFileFormat);

        for (CSVRecord record : csvFileParser.getRecords()) {
            lines.add(record.toMap());
        }

        fileReader.close();
        csvFileParser.close();

        return lines;
    }
}
