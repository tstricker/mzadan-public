package org.expasy.mzadan.core.graph.utils;

import com.google.common.collect.Range;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.expasy.mzadan.core.graph.model.*;
import org.expasy.mzadan.core.ms.*;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;

import java.io.File;
import java.util.*;

/**
 * Created by Thomas on 14.08.2017.
 */


public class VertexUtilsTest {
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph0;

    @BeforeClass
    public static void setUpClass() throws JMzReaderException {
        File file = new File(VertexGeneratorTest.class.getClassLoader().getResource("GraphScoringTestSpectrum.mgf").getFile());
        JMzReader jMzReader = new MgfFile(file);

        jGraph0 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);

        Set<Annotation.Type> annotationTypeSet = new HashSet();
        annotationTypeSet.add(Annotation.Type.NEUTRAL);
        annotationTypeSet.add(Annotation.Type.ADDUCT);
        annotationTypeSet.add(Annotation.Type.ISOTOPE);
        annotationTypeSet.add(Annotation.Type.MULTIMER);

        GraphGeneratorParams graphGeneratorParams = new GraphGeneratorParams();
        graphGeneratorParams.setIntensityPercentThreshold(new Threshold(1.0, Threshold.Limit.HIGHER));
        graphGeneratorParams.setIntensityCountsThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        graphGeneratorParams.setMzRange(Range.atLeast(0.0));
        graphGeneratorParams.setDeisotopingRange(Range.closed(0.0, 100.0));
        graphGeneratorParams.setAnnotationTypeSet(annotationTypeSet);
        graphGeneratorParams.setAdductAnnotationSet(AnnotationSingleton.INSTANCE.getAdductDefaultAnnotationSet());
        graphGeneratorParams.setNeutralAnnotationSet(AnnotationSingleton.INSTANCE.getNeutralDefaultAnnotationSet());
        graphGeneratorParams.setIsotopeAnnotationSet(AnnotationSingleton.INSTANCE.getIsotopeDefaultAnnotationSet());
        graphGeneratorParams.setMassMultiplier(new MzMultiplier(2, MzMultiplier.Type.MASS));
        graphGeneratorParams.setTolerance(new Tolerance(0.005));

        GraphVertexGenerator vertexGenerator = new GraphVertexGenerator(jGraph0, graphGeneratorParams);
        vertexGenerator.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator = new GraphEdgeGenerator(jGraph0, graphGeneratorParams);
        edgeGenerator.process();
    }

    @Test
    public void recursivePeakSetOf() {

        ConnectivityInspector connectivityInspector = new ConnectivityInspector(jGraph0);
        List<Set<Peak>> connectedSetList = connectivityInspector.connectedSets();

        Peak precursor0 = null;
        Peak precursor1 = null;

        for (Set<Peak> peakSet : connectedSetList) {

            for (Peak peak : peakSet) {
                if (peak.getMz() == 200.0) {
                    precursor0 = peak;
                } else if (peak.getMz() == 500.0) {
                    precursor1 = peak;
                }
            }
        }

        //Execute recursive method on Cluster 0
        Set<Peak> outputSet0 = new HashSet();
        Set<Peak> recursionSet0 = new HashSet();
        recursionSet0.add(precursor0);

        VertexUtils.recursivePeakSetOf(jGraph0, recursionSet0, outputSet0);

        //Testing recursive method on Cluster 0
        List<Peak> peakList0 = new ArrayList(connectedSetList.get(0));
        Collections.sort(peakList0, new PeakComparatorByMz());

        Assert.assertEquals(5, peakList0.size());
        Assert.assertEquals(new Peak(200, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), peakList0.get(0));
        Assert.assertEquals(new Peak(217.02655, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), peakList0.get(1));
        Assert.assertEquals(new Peak(221.98195, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), peakList0.get(2));
        Assert.assertEquals(new Peak(237.95589, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), peakList0.get(3));
        Assert.assertEquals(new Peak(243.9639, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList0.get(4));

        List<Peak> recursivePeakList0 = new ArrayList<>(outputSet0);
        Collections.sort(recursivePeakList0, new PeakComparatorByMz());

        Assert.assertEquals(5, peakList0.size());
        Assert.assertEquals(new Peak(200, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), recursivePeakList0.get(0));
        Assert.assertEquals(new Peak(217.02655, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), recursivePeakList0.get(1));
        Assert.assertEquals(new Peak(221.98195, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), recursivePeakList0.get(2));
        Assert.assertEquals(new Peak(237.95589, 7500.0, 75.0, Peak.Type.MONOISOTOPIC), recursivePeakList0.get(3));
        Assert.assertEquals(new Peak(243.9639, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), recursivePeakList0.get(4));

        //Execute recursive method on Cluster 1
        Set<Peak> outputSet1 = new HashSet();
        Set<Peak> recursionSet1 = new HashSet();
        recursionSet1.add(precursor1);

        VertexUtils.recursivePeakSetOf(jGraph0, recursionSet1, outputSet1);

        //Testing the recursive method on Cluster 1
        List<Peak> peakList1 = new ArrayList(connectedSetList.get(1));
        Collections.sort(peakList1, new PeakComparatorByMz());

        Assert.assertEquals(11, peakList1.size());
        Assert.assertEquals(new Peak(484.02606, 1000.0, 10.0, Peak.Type.MONOISOTOPIC), peakList1.get(0));
        Assert.assertEquals(new Peak(500.00000, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), peakList1.get(1));
        Assert.assertEquals(new Peak(515.97394, 1000.0, 10.0, Peak.Type.MONOISOTOPIC), peakList1.get(2));
        Assert.assertEquals(new Peak(517.02655, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(3));
        Assert.assertEquals(new Peak(521.98195, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(4));
        Assert.assertEquals(new Peak(537.95589, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(5));
        Assert.assertEquals(new Peak(543.9639, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), peakList1.get(6));
        Assert.assertEquals(new Peak(559.93784, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), peakList1.get(7));
        Assert.assertEquals(new Peak(575.91178, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), peakList1.get(8));
        Assert.assertEquals(new Peak(961.03683, 100.0, 1.0, Peak.Type.MONOISOTOPIC), peakList1.get(9));
        Assert.assertEquals(new Peak(998.99272, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), peakList1.get(10));

        List<Peak> recursivePeakList1 = new ArrayList<>(outputSet1);
        Collections.sort(recursivePeakList1, new PeakComparatorByMz());

        Assert.assertEquals(9, recursivePeakList1.size());
        Assert.assertEquals(new Peak(500.0, 10000.0, 100.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(0));
        Assert.assertEquals(new Peak(515.97394, 1000.0, 10.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(1));
        Assert.assertEquals(new Peak(517.02655, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(2));
        Assert.assertEquals(new Peak(521.98195, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(3));
        Assert.assertEquals(new Peak(537.95589, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(4));
        Assert.assertEquals(new Peak(543.9639, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(5));
        Assert.assertEquals(new Peak(559.93784, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(6));
        Assert.assertEquals(new Peak(575.91178, 2500.0, 25.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(7));
        Assert.assertEquals(new Peak(998.99272, 5000.0, 50.0, Peak.Type.MONOISOTOPIC), recursivePeakList1.get(8));
    }
}