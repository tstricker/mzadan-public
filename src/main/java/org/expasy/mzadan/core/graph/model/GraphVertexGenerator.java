package org.expasy.mzadan.core.graph.model;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.graph.utils.VertexUtils;
import org.expasy.mzadan.core.ms.*;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;

import java.util.List;

/**
 * Created by Thomas on 06.06.17.
 */
public class GraphVertexGenerator {
    private final SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph;
    private final GraphGeneratorParams graphGeneratorParams;

    public GraphVertexGenerator(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph, GraphGeneratorParams graphGeneratorParams) {
        Preconditions.checkNotNull(jGraph, "JGraph cannot be null.");
        Preconditions.checkNotNull(graphGeneratorParams, "GraphGeneratorParams cannot be null.");

        this.jGraph = jGraph;
        this.graphGeneratorParams = graphGeneratorParams;
    }

    public void process(Spectrum spectrum) {

        List<Peak> peakList = convertToPeakList(spectrum);

        if (graphGeneratorParams.getDeisotopingRange() != null) {
            SpectrumDeisotoping spectrumDeisotoping = new SpectrumDeisotoping(peakList,
                    graphGeneratorParams.getIntensityPercentThreshold(),
                    graphGeneratorParams.getDeisotopingRange(),
                    graphGeneratorParams.getTolerance()
                    );

            peakList = spectrumDeisotoping.process();
        }

        if (graphGeneratorParams.getIntensityPercentThreshold() != null || graphGeneratorParams.getMzRange() != null) {
            SpectrumFiltering spectrumFiltering = new SpectrumFiltering(peakList,
                    graphGeneratorParams.getIntensityPercentThreshold(),
                    graphGeneratorParams.getIntensityCountsThreshold(),
                    graphGeneratorParams.getMzRange());

            peakList = spectrumFiltering.process();
        }

        peakList.stream().forEach(peak -> jGraph.addVertex(peak));
    }

    private List<Peak> convertToPeakList(Spectrum spectrum) {

        if (graphGeneratorParams.getDeisotopingRange() != null && graphGeneratorParams.getIntensityPercentThreshold() != null) {

            Threshold backgroundIonThreshold = new Threshold(graphGeneratorParams.getIntensityPercentThreshold().getValue()
                    * graphGeneratorParams.getDeisotopingRange().lowerEndpoint() / 100, Threshold.Limit.HIGHER);

            return VertexUtils.generateSortedPeakList(spectrum, backgroundIonThreshold);
        } else {
            return VertexUtils.generateSortedPeakList(spectrum);
        }
    }
}

