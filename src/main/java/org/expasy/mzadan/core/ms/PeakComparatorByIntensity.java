package org.expasy.mzadan.core.ms;

import java.util.Comparator;

/**
 * Created by Thomas on 01/11/16.
 */
public class PeakComparatorByIntensity implements Comparator<Peak> {

    @Override
    public int compare(Peak p1, Peak p2) {
        return Double.compare(p1.getIntensity(), p2.getIntensity());
    }
}