package org.expasy.mzadan.core.graph.io;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.graph.model.AnnotatedWeightedEdge;
import org.expasy.mzadan.core.graph.model.GraphGeneratorParams;
import org.expasy.mzadan.core.graph.utils.EdgeUtils;
import org.expasy.mzadan.core.ms.Peak;
import org.expasy.mzadan.core.utils.NumberFormatUtils;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.jgrapht.io.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thomas on 18.06.18.
 */
public class GraphClustersTableExporter {
    private final SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph;
    private final GraphGeneratorParams graphGeneratorParams;

    public GraphClustersTableExporter(SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph, GraphGeneratorParams graphGeneratorParams) {
        Preconditions.checkNotNull(jGraph, "JGraph cannot be null.");
        Preconditions.checkNotNull(graphGeneratorParams, "GraphGeneratorParams cannot be null.");

        this.jGraph = jGraph;
        this.graphGeneratorParams = graphGeneratorParams;
    }

    public void export(File file) {

        try {
            createGraphMlExporter().exportGraph(jGraph, new FileWriter(file.getPath()));
        } catch (ExportException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private GraphMLExporter createGraphMlExporter() {

        ComponentNameProvider<Peak> vertexIdProvider = peak -> NumberFormatUtils.roundToString(peak.getMz(),4);

        ComponentNameProvider<Peak> vertexLabelProvider = peak ->
                "[" + Double.toString(peak.getMz()) + ", " +
                        Double.toString(peak.getIntensity()) + ", " +
                        Double.toString(peak.getRelativeIntensity()) + "]";

        ComponentAttributeProvider<Peak> vertexAttributeProvider = vertex -> {
            Map<String, Attribute> attributeMap = new HashMap<>();
            attributeMap.put("name", DefaultAttribute.createAttribute("Peak-" + vertexLabelProvider.getName(vertex)));
            return attributeMap;
        };

        ComponentNameProvider<AnnotatedWeightedEdge> edgeIdProvider = new IntegerComponentNameProvider<>();

        ComponentNameProvider<AnnotatedWeightedEdge> edgeLabelProvider = annotatedWeightedEdge ->
                "[" + annotatedWeightedEdge.getAnnotation().getMzShift().getName() + ", " +
                        annotatedWeightedEdge.getAnnotation().getMzShift().getValue()+ ", " +
                        NumberFormatUtils.roundToString(EdgeUtils.convertWeightToError(
                               graphGeneratorParams.getTolerance() ,jGraph.getEdgeWeight(annotatedWeightedEdge)), 2) + "]";

        ComponentAttributeProvider<AnnotatedWeightedEdge> edgeAttributeProvider = edge -> {
            Map<String, Attribute> attributeMap = new HashMap<>();
            attributeMap.put("name", DefaultAttribute.createAttribute("Edge-" + edgeLabelProvider.getName(edge)));
            return attributeMap;
        };

        GraphMLExporter<Peak, AnnotatedWeightedEdge> exporter = new GraphMLExporter<>(
                vertexIdProvider, vertexLabelProvider, vertexAttributeProvider, edgeIdProvider,
                edgeLabelProvider, edgeAttributeProvider);

        //exporter.setExportEdgeWeights(true);
        exporter.registerAttribute("name", GraphMLExporter.AttributeCategory.ALL, AttributeType.STRING);

        return exporter;
    }
}
