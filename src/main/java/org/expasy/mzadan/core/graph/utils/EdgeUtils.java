package org.expasy.mzadan.core.graph.utils;


import org.expasy.mzadan.core.ms.Peak;
import org.expasy.mzadan.core.ms.Tolerance;
import org.expasy.mzadan.core.utils.NumberFormatUtils;

import java.util.Collection;

/**
 * Created by Thomas on 22.06.17.
 */
public final class EdgeUtils {

    private EdgeUtils() {
    }

    public static double sumPeakIntensities(Collection<Peak> peakCollection) {

        double summedIntensities = 0.0;

        for (Peak peak : peakCollection) {
            summedIntensities += peak.getIntensity();
        }
        return NumberFormatUtils.round(summedIntensities, 1);
    }

    public static double convertErrorToWeight(Tolerance tolerance, double error) {
        return NumberFormatUtils.round(1 - (error / tolerance.getValue()), 5);
    }

    public static double convertWeightToError(Tolerance tolerance, double weight) {
        return NumberFormatUtils.round(tolerance.getValue() * (1 - weight), 5);
    }
}
