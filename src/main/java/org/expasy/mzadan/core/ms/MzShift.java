package org.expasy.mzadan.core.ms;

import com.google.common.base.Preconditions;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Thomas on 06.06.17.
 */
public class MzShift extends RecursiveTreeObject<MzShift> implements Comparable<MzShift> {
    private final StringProperty name;
    private final DoubleProperty value;

    public MzShift(MzShift mzShift) {
        this(mzShift.getName(), mzShift.getValue());
    }

    public MzShift(String name, double value) {
        Preconditions.checkNotNull(name, "Id cannot be null");

        this.name = new SimpleStringProperty(name);
        this.value = new SimpleDoubleProperty(value);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public double getValue() {
        return value.get();
    }

    public DoubleProperty valueProperty() {
        return value;
    }

    public void setValue(double value) {
        this.value.set(value);
    }

    @Override
    public String toString() {
        return "MzShift{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(MzShift o) {
        int value = Double.compare(this.value.get(), o.getValue());
        return value == 0 ? this.name.get().compareTo(o.getName()) : value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MzShift)) return false;

        MzShift mzShift = (MzShift) o;

        if (!name.equals(mzShift.name)) return false;
        return value.equals(mzShift.value);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
