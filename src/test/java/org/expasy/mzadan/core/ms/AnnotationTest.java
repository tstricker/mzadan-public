package org.expasy.mzadan.core.ms;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Thomas on 09.08.2017.
 */

public class AnnotationTest {
    final Annotation annotation1 = new Annotation(Annotation.Type.NEUTRAL, new MzShift("mzShiftA", -10.0));
    final Annotation annotation2 = new Annotation(Annotation.Type.NEUTRAL, new MzShift("mzShiftA", 0.0));
    final Annotation annotation3 = new Annotation(Annotation.Type.NEUTRAL, new MzShift("mzShiftA", 10.0));
    final Annotation annotation4 = new Annotation(Annotation.Type.NEUTRAL, new MzShift("mzShiftB", 10.0));
    final Annotation annotation5 = new Annotation(Annotation.Type.NEUTRAL, new MzShift("mzShiftC", 10.0));

    @Test(expected = NullPointerException.class)
    public void testIllegalArgumentExceptionId() {
        new Annotation(Annotation.Type.ADDUCT, null);
    }

    @Test
    public void compareTo() {

        Assert.assertEquals(-1, annotation1.compareTo(annotation2));
        Assert.assertEquals(0, annotation1.compareTo(new Annotation(annotation1)));
        Assert.assertEquals(1, annotation2.compareTo(annotation1));
        Assert.assertEquals(-1, annotation2.compareTo(annotation3));
        Assert.assertEquals(0, annotation2.compareTo(new Annotation(annotation2)));
        Assert.assertEquals(1, annotation3.compareTo(annotation2));
        Assert.assertEquals(-1, annotation3.compareTo(annotation4));
        Assert.assertEquals(0, annotation3.compareTo(new Annotation(annotation3)));
        Assert.assertEquals(1, annotation4.compareTo(annotation3));
        Assert.assertEquals(-1, annotation4.compareTo(annotation5));
        Assert.assertEquals(0, annotation4.compareTo(new Annotation(annotation4)));
        Assert.assertEquals(1, annotation5.compareTo(annotation4));
        Assert.assertEquals(0, annotation5.compareTo(new Annotation(annotation5)));
        Assert.assertEquals(1, annotation5.compareTo(annotation1));
    }
}