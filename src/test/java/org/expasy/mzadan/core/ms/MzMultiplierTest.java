package org.expasy.mzadan.core.ms;


import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Thomas on 25.07.18.
 */

public class MzMultiplierTest {

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentException() {
        new MzMultiplier(-1, MzMultiplier.Type.MASS);
    }

    @Test(expected = NullPointerException.class)
    public void testNullPointerException() {
        new MzMultiplier(1, null);
    }

    @Test
    public void compareTo() {
        final MzMultiplier mzMultiplier0 = new MzMultiplier(1, MzMultiplier.Type.CHARGE);
        final MzMultiplier mzMultiplier1 = new MzMultiplier(2, MzMultiplier.Type.CHARGE);
        final MzMultiplier mzMultiplier2 = new MzMultiplier(1, MzMultiplier.Type.MASS);
        final MzMultiplier mzMultiplier3 = new MzMultiplier(2, MzMultiplier.Type.MASS);
        final MzMultiplier mzMultiplier4 = new MzMultiplier(3, MzMultiplier.Type.MASS);

        Assert.assertEquals(-1, mzMultiplier0.compareTo(mzMultiplier1));
        Assert.assertEquals(0, mzMultiplier0.compareTo(new MzMultiplier(mzMultiplier0)));
        Assert.assertEquals(1, mzMultiplier1.compareTo(mzMultiplier0));
        Assert.assertEquals(-1, mzMultiplier1.compareTo(mzMultiplier2));
        Assert.assertEquals(0, mzMultiplier1.compareTo(new MzMultiplier(mzMultiplier1)));
        Assert.assertEquals(1, mzMultiplier2.compareTo(mzMultiplier1));
        Assert.assertEquals(-1, mzMultiplier2.compareTo(mzMultiplier3));
        Assert.assertEquals(0, mzMultiplier2.compareTo(new MzMultiplier(mzMultiplier2)));
        Assert.assertEquals(1, mzMultiplier3.compareTo(mzMultiplier2));
        Assert.assertEquals(-1, mzMultiplier3.compareTo(mzMultiplier4));
        Assert.assertEquals(0, mzMultiplier3.compareTo(new MzMultiplier(mzMultiplier3)));
        Assert.assertEquals(1, mzMultiplier4.compareTo(mzMultiplier3));
        Assert.assertEquals(0, mzMultiplier4.compareTo(new MzMultiplier(mzMultiplier4)));
        Assert.assertEquals(1, mzMultiplier4.compareTo(mzMultiplier0));
    }
}
