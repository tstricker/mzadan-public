package org.expasy.mzadan.fx;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXProgressBar;
import javafx.beans.property.*;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Thomas on 30.08.18.
 */
public class ProgressDialogController implements Initializable {
    private JFXDialog jfxDialog;
    private Task task;

    @FXML
    private JFXProgressBar jfxProgressBar;
    @FXML
    private Label progressLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        progress.addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                jfxProgressBar.setProgress(newValue.doubleValue());
            }
        });

        message.addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                progressLabel.setText(newValue);
            }
        });

        jfxProgressBar.progressProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                if (newValue.doubleValue() == 1.0) {
                    jfxDialog.close();
                }
            }
        });
    }

    private final DoubleProperty progress;
    private final StringProperty message;

    public ProgressDialogController() {
        progress = new SimpleDoubleProperty();
        message = new SimpleStringProperty();
    }

    public void setDialog(JFXDialog jfxDialog) {
        this.jfxDialog = jfxDialog;
    }

    public void setTask(Task<Void> task) {
        progress.bind(task.progressProperty());
        message.bind(task.messageProperty());

        this.task = task;
    }

    @FXML
    public void handleCancelButtonOnAction() {
        task.cancel();
        jfxDialog.close();
    }
}
