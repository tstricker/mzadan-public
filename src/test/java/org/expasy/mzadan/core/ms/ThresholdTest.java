package org.expasy.mzadan.core.ms;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Thomas on 09.08.2017.
 */
public class ThresholdTest {
    @Test
    public void check() throws Exception {

        Assert.assertTrue(new Threshold(1.0, Threshold.Limit.HIGHER).check(1.1));
        Assert.assertTrue(new Threshold(-1.0, Threshold.Limit.HIGHER).check(-0.9));
        Assert.assertTrue(new Threshold(-1.0, Threshold.Limit.HIGHER).check(-1.0));
        Assert.assertTrue(new Threshold(0.0, Threshold.Limit.HIGHER).check(0.0));
        Assert.assertTrue(new Threshold(1.0, Threshold.Limit.HIGHER).check(1.0));
        Assert.assertFalse(new Threshold(1.0, Threshold.Limit.HIGHER).check(0.9));
        Assert.assertFalse(new Threshold(-1.0, Threshold.Limit.HIGHER).check(-1.1));
        Assert.assertTrue(new Threshold(1.0, Threshold.Limit.LOWER).check(0.9));
        Assert.assertTrue(new Threshold(-1.0, Threshold.Limit.LOWER).check(-1.1));
        Assert.assertTrue(new Threshold(-1.0, Threshold.Limit.LOWER).check(-1.0));
        Assert.assertTrue(new Threshold(0.0, Threshold.Limit.LOWER).check(0.0));
        Assert.assertTrue(new Threshold(1.0, Threshold.Limit.LOWER).check(1.0));
        Assert.assertFalse(new Threshold(1.0, Threshold.Limit.LOWER).check(1.1));
        Assert.assertFalse(new Threshold(-1.0, Threshold.Limit.LOWER).check(-0.9));
    }
}