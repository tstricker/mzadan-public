package org.expasy.mzadan.core.graph.model;

import com.google.common.base.Preconditions;
import org.expasy.mzadan.core.ms.Annotation;
import org.jgrapht.graph.DefaultWeightedEdge;

/**
 * Created by Thomas on 06.06.17.
 */
public class AnnotatedWeightedEdge<V> extends DefaultWeightedEdge{
    private V v1;
    private V v2;
    private final Annotation annotation;

    public AnnotatedWeightedEdge(AnnotatedWeightedEdge<V> annotatedWeightedEdge) {
        this(annotatedWeightedEdge.getV1(), annotatedWeightedEdge.getV2(), annotatedWeightedEdge.getAnnotation());
    }

    public AnnotatedWeightedEdge(V v1, V v2, Annotation annotation) {
        Preconditions.checkNotNull(annotation, "Annotation cannot be null");
        this.v1 = v1;
        this.v2 = v2;
        this.annotation = annotation;
    }

    public V getV1() {
        return v1;
    }

    public void setV1(V v1) {
        this.v1 = v1;
    }

    public V getV2() {
        return v2;
    }

    public void setV2(V v2) {
        this.v2 = v2;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

    @Override
    public String toString() {
        return "AnnotatedWeightedEdge{" +
                "v1=" + v1 +
                ", v2=" + v2 +
                ", annotation=" + annotation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnnotatedWeightedEdge<?> edge = (AnnotatedWeightedEdge<?>) o;

        if (v1 != null ? !v1.equals(edge.v1) : edge.v1 != null) return false;
        if (v2 != null ? !v2.equals(edge.v2) : edge.v2 != null) return false;
        return annotation.equals(edge.annotation);
    }

    @Override
    public int hashCode() {
        int result = v1 != null ? v1.hashCode() : 0;
        result = 31 * result + (v2 != null ? v2.hashCode() : 0);
        result = 31 * result + annotation.hashCode();
        return result;
    }
}
