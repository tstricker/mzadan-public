package org.expasy.mzadan.core.ms;

/**
 * Created by Thomas on 06.06.17.
 */
public class Threshold {
    private final double value;
    private final Limit limit;

    public enum Limit {
        HIGHER, LOWER
    }

    public Threshold(Threshold threshold){
        this(threshold.getValue(), threshold.getLimit());
    }

    public Threshold(double value, Limit limit) {
        this.value = value;
        this.limit = limit;
    }

    public boolean check(double current) {

        switch (limit) {
            case HIGHER:
                return current >= value ;
            case LOWER:
                return current <= value;
        }
        return false;
    }

    public double getValue() {
        return value;
    }

    public Limit getLimit() {
        return limit;
    }

    @Override
    public String toString() {
        return "Threshold{" +
                "value=" + value +
                ", limit=" + limit +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Threshold)) return false;

        Threshold threshold = (Threshold) o;

        if (Double.compare(threshold.value, value) != 0) return false;
        return limit == threshold.limit;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(value);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + limit.hashCode();
        return result;
    }
}