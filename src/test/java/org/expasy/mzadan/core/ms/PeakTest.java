package org.expasy.mzadan.core.ms;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by Thomas on 09.08.2017.
 */

public class PeakTest {
    private static Peak peak1;
    private static Peak peak2;
    private static Peak peak3;
    private static Peak peak4;
    private static Peak peak5;

    private static List<Peak> peakList;

    @BeforeClass
    public static void setUpBeforeClass() {
        peak1 = new Peak(250.0, 0.0, 0.0);
        peak2 = new Peak(500.0, 7500.0, 25.0);
        peak3 = new Peak(750.0, 15000.0, 50.0);
        peak4 = new Peak(1000.0, 22500.0, 75.0);
        peak5 = new Peak(1250.0, 30000.0, 100.0);

        peakList = new ArrayList<>();
        peakList.add(peak1);
        peakList.add(peak2);
        peakList.add(peak3);
        peakList.add(peak4);
        peakList.add(peak5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionMz() {
        new Peak(-100.0, 15000.0, 100.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionIntensity() {
        new Peak(100.0, -15000.0, 100.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionRelativeIntensityHigherBound() {
        new Peak(100.0, 15000.0, 101.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionRelativeIntensityLowerBound() {
        new Peak(100.0, 15000.0, -1.0);
    }

    @Test
    public void compareTo() {

        Assert.assertEquals(-1, peak1.compareTo(peak2));
        Assert.assertEquals(0, peak1.compareTo(new Peak(peak1)));
        Assert.assertEquals(1, peak2.compareTo(peak1));
        Assert.assertEquals(-1, peak2.compareTo(peak3));
        Assert.assertEquals(0, peak2.compareTo(new Peak(peak2)));
        Assert.assertEquals(1, peak3.compareTo(peak2));
        Assert.assertEquals(-1, peak3.compareTo(peak4));
        Assert.assertEquals(0, peak3.compareTo(new Peak(peak3)));
        Assert.assertEquals(1, peak4.compareTo(peak3));
        Assert.assertEquals(-1, peak4.compareTo(peak5));
        Assert.assertEquals(0, peak4.compareTo(new Peak(peak4)));
        Assert.assertEquals(1, peak5.compareTo(peak4));
        Assert.assertEquals(0, peak5.compareTo(new Peak(peak5)));
        Assert.assertEquals(1, peak5.compareTo(peak1));
    }

    @Test
    public void PeakComparatorByMz() {

        Collections.sort(peakList, new PeakComparatorByMz());

        Assert.assertEquals(peakList.get(0), peak1);
        Assert.assertEquals(peakList.get(1), peak2);
        Assert.assertEquals(peakList.get(2), peak3);
        Assert.assertEquals(peakList.get(3), peak4);
        Assert.assertEquals(peakList.get(4), peak5);
    }

    @Test
    public void PeakComparatorByintensity() {

        Collections.sort(peakList, new PeakComparatorByIntensity());

        Assert.assertEquals(peakList.get(0), peak1);
        Assert.assertEquals(peakList.get(1), peak2);
        Assert.assertEquals(peakList.get(2), peak3);
        Assert.assertEquals(peakList.get(3), peak4);
        Assert.assertEquals(peakList.get(4), peak5);
    }
}

