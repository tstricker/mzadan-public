package org.expasy.mzadan.core.ms;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Thomas on 07.06.17.
 */
public enum AnnotationSingleton {
    INSTANCE;

    private final Set<Annotation> neutralDefaultAnnotationSet;
    private final Set<Annotation> adductDefaultAnnotationSet;
    private final Set<Annotation> isotopeDefaultAnnotationSet;

    AnnotationSingleton() {
        neutralDefaultAnnotationSet = new TreeSet<>();
        neutralDefaultAnnotationSet.add(new Annotation(Annotation.Type.NEUTRAL, new MzShift("CO", -27.99491)));
        neutralDefaultAnnotationSet.add(new Annotation(Annotation.Type.NEUTRAL, new MzShift("H2O", -18.01056)));
        neutralDefaultAnnotationSet.add(new Annotation(Annotation.Type.NEUTRAL, new MzShift("NH3", -17.02655)));

        adductDefaultAnnotationSet = new TreeSet<>();
        adductDefaultAnnotationSet.add(new Annotation(Annotation.Type.ADDUCT, new MzShift("NH4-H", 17.02655)));
        adductDefaultAnnotationSet.add(new Annotation(Annotation.Type.ADDUCT, new MzShift("Na-H", 21.98195)));
        adductDefaultAnnotationSet.add(new Annotation(Annotation.Type.ADDUCT, new MzShift("K-H", 37.95589)));

        isotopeDefaultAnnotationSet = new TreeSet<>();
        isotopeDefaultAnnotationSet.add(new Annotation(Annotation.Type.ISOTOPE, new MzShift("C13", 1.00336)));
    }
    public Set<Annotation> getAdductDefaultAnnotationSet() {
        return adductDefaultAnnotationSet;
    }

    public Set<Annotation> getNeutralDefaultAnnotationSet() {
        return neutralDefaultAnnotationSet;
    }

    public Set<Annotation> getIsotopeDefaultAnnotationSet() {
        return isotopeDefaultAnnotationSet;
    }
}

