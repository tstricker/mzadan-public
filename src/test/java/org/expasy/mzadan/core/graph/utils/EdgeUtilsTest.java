package org.expasy.mzadan.core.graph.utils;

import org.expasy.mzadan.core.ms.Tolerance;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Thomas on 14.08.2017.
 */
public class EdgeUtilsTest {
    @Test
    public void convertErrorToWeight() throws Exception {

        Assert.assertEquals(1.0, EdgeUtils.convertErrorToWeight(new Tolerance(0.001), 0.0), 0);
        Assert.assertEquals(0.75, EdgeUtils.convertErrorToWeight(new Tolerance(0.001), 0.00025), 0);
        Assert.assertEquals(0.5, EdgeUtils.convertErrorToWeight(new Tolerance(0.001), 0.0005), 0);
        Assert.assertEquals(0.25, EdgeUtils.convertErrorToWeight(new Tolerance(0.001), 0.00075), 0);
        Assert.assertEquals(0.0, EdgeUtils.convertErrorToWeight(new Tolerance(0.001), 0.001), 0);
    }

    @Test
    public void convertWeightToError() throws Exception {

        Assert.assertEquals(0.0, EdgeUtils.convertWeightToError(new Tolerance(0.001), 1.0), 0);
        Assert.assertEquals(0.00025, EdgeUtils.convertWeightToError(new Tolerance(0.001), 0.75), 0);
        Assert.assertEquals(0.0005, EdgeUtils.convertWeightToError(new Tolerance(0.001), 0.5), 0);
        Assert.assertEquals(0.00075, EdgeUtils.convertWeightToError(new Tolerance(0.001), 0.25), 0);
        Assert.assertEquals(0.001, EdgeUtils.convertWeightToError(new Tolerance(0.001), 0.0), 0);
    }
}