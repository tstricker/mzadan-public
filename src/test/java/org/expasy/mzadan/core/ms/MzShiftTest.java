package org.expasy.mzadan.core.ms;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by Thomas on 09.08.2017.
 */

public class MzShiftTest {

    @Test(expected = NullPointerException.class)
    public void testNullPointerExceptionId() {
        new MzShift(null, 0.0);
    }

    @Test
    public void compareTo() {

        final MzShift mzShift0 = new MzShift("mzShiftA", -10.0);
        final MzShift mzShift1 = new MzShift("mzShiftA", 0.0);
        final MzShift mzShift2 = new MzShift("mzShiftA", 10.0);
        final MzShift mzShift3 = new MzShift("mzShiftB", 10.0);
        final MzShift mzShift4 = new MzShift("mzShiftC", 10.0);

        Assert.assertEquals(-1, mzShift0.compareTo(mzShift1));
        Assert.assertEquals(0, mzShift0.compareTo(new MzShift(mzShift0)));
        Assert.assertEquals(1, mzShift1.compareTo(mzShift0));
        Assert.assertEquals(-1, mzShift1.compareTo(mzShift2));
        Assert.assertEquals(0, mzShift1.compareTo(new MzShift(mzShift1)));
        Assert.assertEquals(1, mzShift2.compareTo(mzShift1));
        Assert.assertEquals(-1, mzShift2.compareTo(mzShift3));
        Assert.assertEquals(0, mzShift2.compareTo(new MzShift(mzShift2)));
        Assert.assertEquals(1, mzShift3.compareTo(mzShift2));
        Assert.assertEquals(-1, mzShift3.compareTo(mzShift4));
        Assert.assertEquals(0, mzShift3.compareTo(new MzShift(mzShift3)));
        Assert.assertEquals(1, mzShift4.compareTo(mzShift3));
        Assert.assertEquals(0, mzShift4.compareTo(new MzShift(mzShift4)));
        Assert.assertEquals(1, mzShift4.compareTo(mzShift0));
    }
}