package org.expasy.mzadan.core.graph.model;

import com.google.common.collect.Range;
import org.expasy.mzadan.core.ms.Annotation;
import org.expasy.mzadan.core.ms.MzMultiplier;
import org.expasy.mzadan.core.ms.Threshold;
import org.expasy.mzadan.core.ms.Tolerance;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Thomas on 12.01.18.
 */
public class GraphGeneratorParams {
    private Threshold intensityPercentThreshold;
    private Threshold intensityCountsThreshold;
    private Range<Double> mzRange;
    private Range<Double> deisotopingRange;
    private Tolerance tolerance;
    private Set<Annotation.Type> annotationTypeSet;
    private Set<Annotation> neutralAnnotationSet;
    private Set<Annotation> adductAnnotationSet;
    private Set<Annotation> isotopeAnnotationSet;
    private MzMultiplier massMultiplier;
    private MzMultiplier chargeMultiplier;
    private Threshold compoundPeakIntensityPercentThreshold;
    private Threshold compoundPeakIntensityCountsThreshold;
    private Threshold clusterConnectivityIndexThreshold;
    private Threshold clusterIntensityCoverageThreshold;
    private Threshold clusterCountCoverageThreshold;

    public GraphGeneratorParams() {
        annotationTypeSet = new HashSet<>();
        neutralAnnotationSet = new TreeSet<>();
        adductAnnotationSet = new TreeSet<>();
        isotopeAnnotationSet = new TreeSet<>();
    }

    public Threshold getIntensityPercentThreshold() {
        return intensityPercentThreshold;
    }

    public void setIntensityPercentThreshold(Threshold intensityPercentThreshold) {
        this.intensityPercentThreshold = intensityPercentThreshold;
    }

    public Threshold getIntensityCountsThreshold() {
        return intensityCountsThreshold;
    }

    public void setIntensityCountsThreshold(Threshold intensityCountsThreshold) {
        this.intensityCountsThreshold = intensityCountsThreshold;
    }

    public Range<Double> getMzRange() {
        return mzRange;
    }

    public void setMzRange(Range<Double> mzRange) {
        this.mzRange = mzRange;
    }

    public Range<Double> getDeisotopingRange() {
        return deisotopingRange;
    }

    public void setDeisotopingRange(Range<Double> deisotopingRange) {
        this.deisotopingRange = deisotopingRange;
    }

    public Tolerance getTolerance() {
        return tolerance;
    }

    public void setTolerance(Tolerance tolerance) {
        this.tolerance = tolerance;
    }

    public Set<Annotation.Type> getAnnotationTypeSet() {
        return annotationTypeSet;
    }

    public void setAnnotationTypeSet(Set<Annotation.Type> annotationTypeSet) {
        this.annotationTypeSet = annotationTypeSet;
    }

    public Set<Annotation> getNeutralAnnotationSet() {
        return neutralAnnotationSet;
    }

    public void setNeutralAnnotationSet(Set<Annotation> neutralAnnotationSet) {
        this.neutralAnnotationSet = neutralAnnotationSet;
    }

    public Set<Annotation> getAdductAnnotationSet() {
        return adductAnnotationSet;
    }

    public void setAdductAnnotationSet(Set<Annotation> adductAnnotationSet) {
        this.adductAnnotationSet = adductAnnotationSet;
    }

    public Set<Annotation> getIsotopeAnnotationSet() {
        return isotopeAnnotationSet;
    }

    public void setIsotopeAnnotationSet(Set<Annotation> isotopeAnnotationSet) {
        this.isotopeAnnotationSet = isotopeAnnotationSet;
    }

    public MzMultiplier getMassMultiplier() {
        return massMultiplier;
    }

    public void setMassMultiplier(MzMultiplier massMultiplier) {
        this.massMultiplier = massMultiplier;
    }

    public MzMultiplier getChargeMultiplier() {
        return chargeMultiplier;
    }

    public void setChargeMultiplier(MzMultiplier chargeMultiplier) {
        this.chargeMultiplier = chargeMultiplier;
    }

    public Threshold getCompoundPeakIntensityPercentThreshold() {
        return compoundPeakIntensityPercentThreshold;
    }

    public void setCompoundPeakIntensityPercentThreshold(Threshold compoundPeakIntensityPercentThreshold) {
        this.compoundPeakIntensityPercentThreshold = compoundPeakIntensityPercentThreshold;
    }

    public Threshold getCompoundPeakIntensityCountsThreshold() {
        return compoundPeakIntensityCountsThreshold;
    }

    public void setCompoundPeakIntensityCountsThreshold(Threshold compoundPeakIntensityCountsThreshold) {
        this.compoundPeakIntensityCountsThreshold = compoundPeakIntensityCountsThreshold;
    }

    public Threshold getClusterConnectivityIndexThreshold() {
        return clusterConnectivityIndexThreshold;
    }

    public void setClusterConnectivityIndexThreshold(Threshold clusterConnectivityIndexThreshold) {
        this.clusterConnectivityIndexThreshold = clusterConnectivityIndexThreshold;
    }

    public Threshold getClusterIntensityCoverageThreshold() {
        return clusterIntensityCoverageThreshold;
    }

    public void setClusterIntensityCoverageThreshold(Threshold clusterIntensityCoverageThreshold) {
        this.clusterIntensityCoverageThreshold = clusterIntensityCoverageThreshold;
    }

    public Threshold getClusterCountCoverageThreshold() {
        return clusterCountCoverageThreshold;
    }

    public void setClusterCountCoverageThreshold(Threshold clusterCountCoverageThreshold) {
        this.clusterCountCoverageThreshold = clusterCountCoverageThreshold;
    }
}
