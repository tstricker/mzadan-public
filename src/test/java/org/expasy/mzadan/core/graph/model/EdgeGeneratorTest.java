package org.expasy.mzadan.core.graph.model;

import com.google.common.collect.Range;
import org.expasy.mzadan.core.graph.utils.EdgeUtils;
import org.expasy.mzadan.core.ms.*;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Thomas on 10.08.2017.
 */


public class EdgeGeneratorTest {
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph0;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph1;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph2;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph3;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph4;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph5;
    private static SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph6;

    @BeforeClass
    public static void setUpClass() throws JMzReaderException {
        File file = new File(EdgeGeneratorTest.class.getClassLoader().getResource("EdgeGeneratorTestSpectrum.mgf").getFile());
        JMzReader jMzReader = new MgfFile(file);

        jGraph0 = new SimpleDirectedWeightedGraph(AnnotatedWeightedEdge.class);

        Set<Annotation.Type> annotationTypeSet = new HashSet();
        annotationTypeSet.add(Annotation.Type.NEUTRAL);
        annotationTypeSet.add(Annotation.Type.ADDUCT);
        annotationTypeSet.add(Annotation.Type.ISOTOPE);
        annotationTypeSet.add(Annotation.Type.MULTIMER);

        GraphGeneratorParams defaultParams = new GraphGeneratorParams();
        defaultParams.setIntensityPercentThreshold(new Threshold(1, Threshold.Limit.HIGHER));
        defaultParams.setIntensityCountsThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        defaultParams.setMzRange(Range.atLeast(0.0));
        defaultParams.setDeisotopingRange(Range.closed(0.0, 100.0));
        defaultParams.setAdductAnnotationSet(AnnotationSingleton.INSTANCE.getAdductDefaultAnnotationSet());
        defaultParams.setNeutralAnnotationSet(AnnotationSingleton.INSTANCE.getNeutralDefaultAnnotationSet());
        defaultParams.setIsotopeAnnotationSet(AnnotationSingleton.INSTANCE.getIsotopeDefaultAnnotationSet());
        defaultParams.setMassMultiplier(new MzMultiplier(2, MzMultiplier.Type.MASS));
        defaultParams.setTolerance(new Tolerance(0.005));

        GraphVertexGenerator vertexGenerator0 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator0.process(jMzReader.getSpectrumByIndex(1));

        jGraph1 = (SimpleDirectedWeightedGraph) jGraph0.clone();
        jGraph2 = (SimpleDirectedWeightedGraph) jGraph0.clone();
        jGraph3 = (SimpleDirectedWeightedGraph) jGraph0.clone();
        jGraph4 = (SimpleDirectedWeightedGraph) jGraph0.clone();
        jGraph5 = (SimpleDirectedWeightedGraph) jGraph0.clone();
        jGraph6 = (SimpleDirectedWeightedGraph) jGraph0.clone();

        //Generating edges for NEUTRAL annotations with default parameters.
        Set<Annotation.Type> annotationSet0 = new HashSet();
        annotationSet0.add(Annotation.Type.NEUTRAL);

        defaultParams.setAnnotationTypeSet(annotationSet0);

        GraphEdgeGenerator edgeGenerator0 = new GraphEdgeGenerator(jGraph0, defaultParams);
        edgeGenerator0.process();

        //Generating edges for ADDUCT annotations with default parameters.
        Set<Annotation.Type> annotationSet1 = new HashSet();
        annotationSet1.add(Annotation.Type.ADDUCT);

        defaultParams.setAnnotationTypeSet(annotationSet1);

        GraphVertexGenerator vertexGenerator1 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator1.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator1 = new GraphEdgeGenerator(jGraph1, defaultParams);
        edgeGenerator1.process();

        //Generating edges for ISOTOPE annotations with default parameters.
        Set<Annotation.Type> annotationSet2 = new HashSet();
        annotationSet2.add(Annotation.Type.ISOTOPE);

        defaultParams.setAnnotationTypeSet(annotationSet2);

        GraphVertexGenerator vertexGenerator2 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator2.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator2 = new GraphEdgeGenerator(jGraph2, defaultParams);
        edgeGenerator2.process();

        Assert.assertEquals(0, jGraph2.edgeSet().size());

        //Generating edges for MULTIMER annotations with default parameters.
        Set<Annotation.Type> annotationSet3 = new HashSet();
        annotationSet3.add(Annotation.Type.MULTIMER);

        defaultParams.setAnnotationTypeSet(annotationSet3);
        defaultParams.setIntensityPercentThreshold(new Threshold(1, Threshold.Limit.HIGHER));

        GraphVertexGenerator vertexGenerator3 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator3.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator3 = new GraphEdgeGenerator(jGraph3, defaultParams);
        edgeGenerator3.process();

        Assert.assertEquals(1, jGraph3.edgeSet().size());

        //Generating edges for all annotations with a tolerance of 0 mmu.
        Set<Annotation.Type> annotationSet4 = new HashSet();
        annotationSet4.add(Annotation.Type.NEUTRAL);
        annotationSet4.add(Annotation.Type.ADDUCT);
        annotationSet4.add(Annotation.Type.ISOTOPE);
        annotationSet4.add(Annotation.Type.MULTIMER);

        defaultParams.setAnnotationTypeSet(annotationSet4);
        defaultParams.setTolerance(new Tolerance(0));

        GraphVertexGenerator vertexGenerator4 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator4.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator4 = new GraphEdgeGenerator(jGraph4, defaultParams);
        edgeGenerator4.process();

        Assert.assertEquals(15, jGraph4.edgeSet().size());

        //Generating edges for all annotations with a tolerance of 5 mmu.
        Set<Annotation.Type> annotationSet5 = new HashSet();
        annotationSet5.add(Annotation.Type.NEUTRAL);
        annotationSet5.add(Annotation.Type.ADDUCT);
        annotationSet5.add(Annotation.Type.ISOTOPE);
        annotationSet5.add(Annotation.Type.MULTIMER);

        defaultParams.setAnnotationTypeSet(annotationSet5);
        defaultParams.setTolerance(new Tolerance(0.005));

        GraphVertexGenerator vertexGenerator5 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator5.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator5 = new GraphEdgeGenerator(jGraph5, defaultParams);
        edgeGenerator5.process();


        //Generating edges for all annotations with a tolerance of 10 mmu.
        Set<Annotation.Type> annotationSet6 = new HashSet();
        annotationSet6.add(Annotation.Type.NEUTRAL);
        annotationSet6.add(Annotation.Type.ADDUCT);
        annotationSet6.add(Annotation.Type.ISOTOPE);
        annotationSet6.add(Annotation.Type.MULTIMER);

        defaultParams.setAnnotationTypeSet(annotationSet6);
        defaultParams.setTolerance(new Tolerance(0.010));

        GraphVertexGenerator vertexGenerator6 = new GraphVertexGenerator(jGraph0, defaultParams);
        vertexGenerator6.process(jMzReader.getSpectrumByIndex(1));

        GraphEdgeGenerator edgeGenerator6 = new GraphEdgeGenerator(jGraph6, defaultParams);
        edgeGenerator6.process();

    }

    @Test
    public void process() throws Exception {

        //Testing the annotation and filtering of edges with a tolerance for neutral losses only.
        Assert.assertEquals(8, jGraph0.edgeSet().size());

        for (AnnotatedWeightedEdge edge : jGraph0.edgeSet()) {
            Assert.assertEquals(Annotation.Type.NEUTRAL, edge.getAnnotation().getType());
            Assert.assertEquals(0.0, EdgeUtils.convertWeightToError(new Tolerance(0.0),
                    jGraph0.getEdgeWeight(edge)), 0.0);
        }

        //Testing the annotation and filtering of edges for adducts only.
        Assert.assertEquals(7, jGraph1.edgeSet().size());

        for (AnnotatedWeightedEdge edge : jGraph1.edgeSet()) {
            Assert.assertEquals(Annotation.Type.ADDUCT, edge.getAnnotation().getType());
            Assert.assertEquals(0.0, EdgeUtils.convertWeightToError(new Tolerance(0.0),
                    jGraph1.getEdgeWeight(edge)), 0.0);
        }

        //Testing the annotation and filtering of edges for isotopes only.
        for (AnnotatedWeightedEdge edge : jGraph2.edgeSet()) {
            Assert.assertEquals(Annotation.Type.ISOTOPE, edge.getAnnotation().getType());
            Assert.assertEquals(0.0, EdgeUtils.convertWeightToError(new Tolerance(0.0),
                    jGraph2.getEdgeWeight(edge)), 0.0);
        }

        //Testing the annotation and filtering of edges for Multimer only.
        for (AnnotatedWeightedEdge edge : jGraph3.edgeSet()) {
            Assert.assertEquals(Annotation.Type.MULTIMER, edge.getAnnotation().getType());
            Assert.assertEquals(0.0, EdgeUtils.convertWeightToError(new Tolerance(0.0),
                    jGraph3.getEdgeWeight(edge)), 0.0);
        }

        //Testing the annotation and filtering of edges with a tolerance of 0 mmu.

        for (AnnotatedWeightedEdge edge : jGraph4.edgeSet()) {
            Assert.assertTrue(EdgeUtils.convertWeightToError(new Tolerance(0.0),
                    jGraph4.getEdgeWeight(edge)) <= 0.0);
        }

        //Testing the annotation and filtering of edges with a tolerance of 5 mmu.
        Assert.assertEquals(16, jGraph5.edgeSet().size());

        for (AnnotatedWeightedEdge edge : jGraph5.edgeSet()) {
            Assert.assertTrue(EdgeUtils.convertWeightToError(new Tolerance(0.001),
                    jGraph5.getEdgeWeight(edge)) <= 0.001);
        }

        //Testing the annotation and filtering of edges with a tolerance of 10 mmu.
        Assert.assertEquals(17, jGraph6.edgeSet().size());

        for (AnnotatedWeightedEdge edge : jGraph6.edgeSet()) {
            Assert.assertTrue(EdgeUtils.convertWeightToError(new Tolerance(0.002),
                    jGraph6.getEdgeWeight(edge)) <= 0.002);
        }
    }
}