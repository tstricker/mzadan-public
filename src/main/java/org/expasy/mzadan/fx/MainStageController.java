package org.expasy.mzadan.fx;


import com.google.common.collect.Range;
import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.*;
import org.expasy.mzadan.MainFx;
import org.expasy.mzadan.core.graph.io.GraphAnnotationsTableExporter;
import org.expasy.mzadan.core.graph.io.GraphClustersTableExporter;
import org.expasy.mzadan.core.graph.io.GraphCandidatesTableExporter;
import org.expasy.mzadan.core.graph.model.AnnotatedWeightedEdge;
import org.expasy.mzadan.core.graph.model.GraphEdgeGenerator;
import org.expasy.mzadan.core.graph.model.GraphGeneratorParams;
import org.expasy.mzadan.core.graph.model.GraphVertexGenerator;
import org.expasy.mzadan.core.io.CsvFileReader;
import org.expasy.mzadan.core.ms.*;
import org.expasy.mzadan.core.ms.io.SpectrumRecord;
import org.expasy.mzadan.core.ms.io.SpectrumRecordExport;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainStageController implements Initializable {

    // StackPane from the mainPane.
    @FXML
    private StackPane mainStackPane;

    // AnchorPane the from the StackPane.
    @FXML
    private AnchorPane homeAnchorPane;
    @FXML
    private AnchorPane projectAnchorPane;
    @FXML
    private AnchorPane projectVisualizationAnchorPane;
    @FXML
    private AnchorPane projectExportAnchorPane;
    @FXML
    private AnchorPane settingsAnchorPane;
    @FXML
    private AnchorPane parametersSettingsAnchorPane;
    @FXML
    private AnchorPane annotationsSettingsAnchorPane;
    @FXML
    private AnchorPane identificationParametersAnchorPane;

    //JFXComboBox from the project-export pane.
    @FXML
    private JFXComboBox<Label> inputProjectJFXCombobox;
    @FXML
    private JFXComboBox<Label> outputProjectJFXCombobox;

    // JFXRadioButton from the project-export pane.
    @FXML
    JFXRadioButton annotationsBaseDirectoryJFXRadioButton;
    @FXML
    JFXRadioButton annotationsNewDirectoryJFXRadioButton;
    @FXML
    JFXRadioButton candidatesBaseDirectoryJFXRadioButton;
    @FXML
    JFXRadioButton candidatesNewDirectoryJFXRadioButton;
    @FXML
    JFXRadioButton graphsBaseDirectoryJFXRadioButton;
    @FXML
    JFXRadioButton graphsNewDirectoryJFXRadioButton;

    // JFXButton from the project-export pane.
    @FXML
    private JFXButton exportProjectJFXButton;

    // JFXButton from the settings-annotation pane.
    @FXML
    private JFXButton adductAnnotationsEditJFXButton;
    @FXML
    private JFXButton neutralAnnotationsEditJFXButton;
    @FXML
    private JFXButton isotopeAnnotationsEditJFXButton;
    @FXML
    private JFXButton adductAnnotationsImportJFXButton;
    @FXML
    private JFXButton neutralAnnotationsImportJFXButton;
    @FXML
    private JFXButton isotopeAnnotationsImportJFXButton;
    @FXML
    private JFXButton adductAnnotationsResetJFXButton;
    @FXML
    private JFXButton neutralAnnotationsResetJFXButton;
    @FXML
    private JFXButton isotopeAnnotationsResetJFXButton;

    // JFXToggleButton from the project-export pane.
    @FXML
    private JFXToggleButton annotationsExportToggleButton;
    @FXML
    private JFXToggleButton compoundsExportToggleButton;
    @FXML
    private JFXToggleButton clustersExportToggleButton;

    // JFXToggleButton from the settings-parameters pane.
    @FXML
    private JFXToggleButton intensityThresholdToggleButton;
    @FXML
    private JFXToggleButton mzRangeToggleButton;
    @FXML
    private JFXToggleButton deisotopingRangeToggleButton;
    @FXML
    private JFXToggleButton mmuToleranceToggleButton;

    // JFXToggleButton from the settings-annotation pane.
    @FXML
    private JFXToggleButton adductAnnotationsJFXToggleButton;
    @FXML
    private JFXToggleButton neutralAnnotationsJFXToggleButton;
    @FXML
    private JFXToggleButton isotopeAnnotationsJFXToggleButton;
    @FXML
    private JFXToggleButton multimerAnnotationsJFXToggleButton;
    @FXML
    private JFXToggleButton multichargeAnnotationsJFXToggleButton;

    // JFXToggleButton from the settings-identification pane.
    @FXML
    private JFXToggleButton candidateIntensityJFXToggleButton;
    @FXML
    private JFXToggleButton clusterConnectivityIndexJFXToggleButton;
    @FXML
    private JFXToggleButton clusterIntensityCoverageJFXToggleButton;
    @FXML
    private JFXToggleButton clusterCountCoverageJFXToggleButton;

    // JFXTextField from the project-export pane.
    @FXML
    private JFXTextField inputJFXTextField;
    @FXML
    private JFXTextField outputJFXTextField;

    // JFXTextField from the settings-parameters pane.
    @FXML
    private JFXTextField intensityPercentThresholdJFKTextField;
    @FXML
    private JFXTextField intensityCountsThresholdJFKTextField;
    @FXML
    private JFXTextField mzRangeMinJFXTextField;
    @FXML
    private JFXTextField mzRangeMaxJFXTextField;
    @FXML
    private JFXTextField deisotopingRangeMinJFXTextField;
    @FXML
    private JFXTextField deisotopingRangeMaxJFXTextField;
    @FXML
    private JFXTextField mmuToleranceJFXTextField;

    // JFXTextField from the settings-annotation pane.
    @FXML
    private JFXTextField multimerAnnotationsJFXTextField;
    @FXML
    private JFXTextField multichargeAnnotationsJFXTextField;

    // JFXTextField from the settings-identification pane.
    @FXML
    private JFXTextField candidateIntensityPercentJFXTextField;
    @FXML
    private JFXTextField candidateIntensityCountsJFXTextField;
    @FXML
    private JFXTextField clusterConnectivityIndexJFXTextField;
    @FXML
    private JFXTextField clusterIntensityCoverageJFXTextField;
    @FXML
    private JFXTextField clusterCountCoverageJFXTextField;

    private final static String UNAUTHORIZED_VALUE_STYLE = "-fx-text-fill: #D32F2F; -jfx-focus-color: #F44336; -jfx-unfocus-color: #E57373;";

    private void addProjectFileListener(ObjectProperty<File> file, JFXTextField textField) {
        file.addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue && newValue != null) {
                textField.setText(newValue.getAbsolutePath());
            }
        });
    }

    private void addJFXTextFieldProjectFileListener(JFXTextField textField, JFXButton button) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                if (isProjectExportable()) {
                    button.setDisable(false);
                } else {
                    button.setDisable(true);
                }
            }
        });
    }

    private void addJFXToggleButtonProjectExportListener(JFXToggleButton toggleButton, JFXButton button) {
        toggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                if (isProjectExportable()) {
                    button.setDisable(false);
                } else {
                    button.setDisable(true);
                }
            }
        });
    }

    private void addJFXTextFieldKeyTypeEventFilter(JFXTextField textField, String regex) {
        textField.addEventFilter(KeyEvent.KEY_TYPED, ev -> {
            if (!ev.getCharacter().matches(regex)) {
                ev.consume();
            }
        });
    }

    private void addJFXTextFieldValueFocusedListener(JFXTextField textField) {
        textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                if (textField.getText().isEmpty()) {
                    textField.setText("0");
                }
            }
        });
    }

    private void addJFXTextFieldValueListener(JFXTextField textField, String regex) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                if (!newValue.matches(regex)) {
                    textField.setStyle(UNAUTHORIZED_VALUE_STYLE);
                } else {
                    textField.setStyle(null);
                }
            }
        });
    }

    private void addJFXTextFieldRangeValueListeners(JFXTextField minValueTextField, JFXTextField maxValueTextField) {

        minValueTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                try {
                    if (!minValueTextField.getText().isEmpty() && !maxValueTextField.getText().isEmpty()) {
                        if (Double.parseDouble(minValueTextField.getText()) > Double.parseDouble(maxValueTextField.getText())) {
                            minValueTextField.setStyle(UNAUTHORIZED_VALUE_STYLE);
                            maxValueTextField.setStyle(UNAUTHORIZED_VALUE_STYLE);
                        } else {
                            minValueTextField.setStyle(null);
                            maxValueTextField.setStyle(null);
                        }
                    }
                } catch (NumberFormatException e) {
                    minValueTextField.setStyle(UNAUTHORIZED_VALUE_STYLE);
                }
            }
        });

        maxValueTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                try {
                    if (!minValueTextField.getText().isEmpty() && !maxValueTextField.getText().isEmpty()) {
                        if (Double.parseDouble(maxValueTextField.getText()) < Double.parseDouble(minValueTextField.getText())) {
                            minValueTextField.setStyle(UNAUTHORIZED_VALUE_STYLE);
                            maxValueTextField.setStyle(UNAUTHORIZED_VALUE_STYLE);
                        } else {
                            minValueTextField.setStyle(null);
                            maxValueTextField.setStyle(null);
                        }
                    }
                } catch (NumberFormatException e) {
                    minValueTextField.setStyle(UNAUTHORIZED_VALUE_STYLE);
                }
            }
        });
    }

    private void addJFXComboBoxListener(JFXComboBox comboBox, JFXTextField textField) {
        comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                textField.clear();
            }
        });
    }

    private void addJFXToggleButtonListener(JFXToggleButton jfxToggleButton, Node node) {
        jfxToggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue) {
                if (jfxToggleButton.isSelected()) {
                    node.setDisable(false);
                } else {
                    node.setDisable(true);
                }
            }
        });
    }

    private void addToolTipToJFXButton(JFXButton jfxButton, String message) {
        jfxButton.setTooltip(new Tooltip(message));
    }

    private boolean isProjectExportable() {

        if (inputJFXTextField.getText().equals("")) {
            return false;
        }
        if (outputJFXTextField.getText().equals("")) {
            return false;
        }
        if (!annotationsExportToggleButton.isSelected() && !compoundsExportToggleButton.isSelected() && !clustersExportToggleButton.isSelected()) {
            return false;
        }

        return true;
    }

    public void initialize(URL location, ResourceBundle resources) {

        // Adding items in JFXComboBox in project-export pane.
        inputProjectJFXCombobox.getItems().addAll(new Label("File"), new Label("Directory"));
        inputProjectJFXCombobox.getSelectionModel().select(0);
        outputProjectJFXCombobox.getItems().addAll(new Label("Unique"));
        outputProjectJFXCombobox.getSelectionModel().select(0);

        // Listening to File in JFXTextField in project-export pane.
        addProjectFileListener(inputFile, inputJFXTextField);
        addProjectFileListener(outputFile, outputJFXTextField);

        // Listening to empty value in JFXTextField in project-export pane.
        addJFXTextFieldProjectFileListener(inputJFXTextField, exportProjectJFXButton);
        addJFXTextFieldProjectFileListener(outputJFXTextField, exportProjectJFXButton);

        // Listening to value in JFXToggleButton in project-export pane.
        addJFXToggleButtonProjectExportListener(annotationsExportToggleButton, exportProjectJFXButton);
        addJFXToggleButtonProjectExportListener(compoundsExportToggleButton, exportProjectJFXButton);
        addJFXToggleButtonProjectExportListener(clustersExportToggleButton, exportProjectJFXButton);

        // Listening to value in JFXCombobox in project-export pane.
        addJFXComboBoxListener(inputProjectJFXCombobox, inputJFXTextField);
        addJFXComboBoxListener(inputProjectJFXCombobox, outputJFXTextField);

        // Listening to KeyTypeEvent in JFXTextField in settings-parameters pane.
        addJFXTextFieldKeyTypeEventFilter(intensityPercentThresholdJFKTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(intensityCountsThresholdJFKTextField, "\\d");
        addJFXTextFieldKeyTypeEventFilter(mzRangeMinJFXTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(mzRangeMaxJFXTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(deisotopingRangeMinJFXTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(deisotopingRangeMaxJFXTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(mmuToleranceJFXTextField, "\\d|\\.");

        // Listening to KeyTypeEvent in JFXTextField in settings-annotation pane.
        addJFXTextFieldKeyTypeEventFilter(multimerAnnotationsJFXTextField, "\\d");
        addJFXTextFieldKeyTypeEventFilter(multichargeAnnotationsJFXTextField, "\\d");

        // Listening to KeyTypeEvent in JFXTextField in settings-identification pane.
        addJFXTextFieldKeyTypeEventFilter(candidateIntensityPercentJFXTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(candidateIntensityCountsJFXTextField, "\\d");
        addJFXTextFieldKeyTypeEventFilter(clusterConnectivityIndexJFXTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(clusterIntensityCoverageJFXTextField, "\\d|\\.");
        addJFXTextFieldKeyTypeEventFilter(clusterCountCoverageJFXTextField, "\\d|\\.");

        // Listening to empty value in JFXTextField in settings-parameters pane.
        addJFXTextFieldValueFocusedListener(intensityPercentThresholdJFKTextField);
        addJFXTextFieldValueFocusedListener(intensityCountsThresholdJFKTextField);
        addJFXTextFieldValueFocusedListener(mzRangeMinJFXTextField);
        addJFXTextFieldValueFocusedListener(mzRangeMaxJFXTextField);
        addJFXTextFieldValueFocusedListener(deisotopingRangeMinJFXTextField);
        addJFXTextFieldValueFocusedListener(deisotopingRangeMaxJFXTextField);
        addJFXTextFieldValueFocusedListener(mmuToleranceJFXTextField);

        // Listening value empty in JFXTextField in settings-annotation pane.
        addJFXTextFieldValueFocusedListener(multimerAnnotationsJFXTextField);
        addJFXTextFieldValueFocusedListener(multichargeAnnotationsJFXTextField);

        // Listening value empty in JFXTextField in settings-analysis pane.
        addJFXTextFieldValueFocusedListener(candidateIntensityPercentJFXTextField);
        addJFXTextFieldValueFocusedListener(candidateIntensityCountsJFXTextField);
        addJFXTextFieldValueFocusedListener(clusterConnectivityIndexJFXTextField);
        addJFXTextFieldValueFocusedListener(clusterIntensityCoverageJFXTextField);
        addJFXTextFieldValueFocusedListener(clusterCountCoverageJFXTextField);

        // Checking value in JFXTextField in settings-parameters pane.
        addJFXTextFieldValueListener(intensityPercentThresholdJFKTextField, "^(\\d{0,2}(\\.\\d+)?|100(\\.0+)?)$");
        addJFXTextFieldValueListener(mzRangeMinJFXTextField, "^(\\d*(\\.\\d+)?)$");
        addJFXTextFieldValueListener(mzRangeMaxJFXTextField, "^(\\d*(\\.\\d+)?)$");
        addJFXTextFieldValueListener(deisotopingRangeMinJFXTextField, "^(\\d{0,2}(\\.\\d+)?|100(\\.0+)?)$");
        addJFXTextFieldValueListener(deisotopingRangeMaxJFXTextField, "^(\\d{0,2}(\\.\\d+)?|100(\\.0+)?)$");
        addJFXTextFieldValueListener(mmuToleranceJFXTextField, "^(\\d*(\\.\\d+)?)$");

        // Checking value in JFXTextField in settings-analysis pane.
        addJFXTextFieldValueListener(candidateIntensityPercentJFXTextField, "^(\\d{0,2}(\\.\\d+)?|100(\\.0+)?)$");
        addJFXTextFieldValueListener(candidateIntensityCountsJFXTextField, "^(\\d*(\\.\\d+)?)$");
        addJFXTextFieldValueListener(clusterConnectivityIndexJFXTextField, "^(\\d{0,2}(\\.\\d+)?|100(\\.0+)?)$");
        addJFXTextFieldValueListener(clusterIntensityCoverageJFXTextField, "^(\\d{0,2}(\\.\\d+)?|100(\\.0+)?)$");
        addJFXTextFieldValueListener(clusterCountCoverageJFXTextField, "^(\\d{0,2}(\\.\\d+)?|100(\\.0+)?)$");

        // Checking Min-Max values in JFXTextField in settings-parameters pane.
        addJFXTextFieldRangeValueListeners(mzRangeMinJFXTextField, mzRangeMaxJFXTextField);
        addJFXTextFieldRangeValueListeners(deisotopingRangeMinJFXTextField, deisotopingRangeMaxJFXTextField);

        // Binding JFXToggleButton to JFXTextField in project-export pane.
        addJFXToggleButtonListener(annotationsExportToggleButton, annotationsBaseDirectoryJFXRadioButton);
        addJFXToggleButtonListener(annotationsExportToggleButton, annotationsNewDirectoryJFXRadioButton);
        addJFXToggleButtonListener(compoundsExportToggleButton, candidatesBaseDirectoryJFXRadioButton);
        addJFXToggleButtonListener(compoundsExportToggleButton, candidatesNewDirectoryJFXRadioButton);
        addJFXToggleButtonListener(clustersExportToggleButton, graphsBaseDirectoryJFXRadioButton);
        addJFXToggleButtonListener(clustersExportToggleButton, graphsNewDirectoryJFXRadioButton);

        // Binding JFXToggleButton to JFXTextField in settings-parameters pane.
        addJFXToggleButtonListener(intensityThresholdToggleButton, intensityPercentThresholdJFKTextField);
        addJFXToggleButtonListener(intensityThresholdToggleButton, intensityCountsThresholdJFKTextField);
        addJFXToggleButtonListener(mzRangeToggleButton, mzRangeMinJFXTextField);
        addJFXToggleButtonListener(mzRangeToggleButton, mzRangeMaxJFXTextField);
        addJFXToggleButtonListener(deisotopingRangeToggleButton, deisotopingRangeMinJFXTextField);
        addJFXToggleButtonListener(deisotopingRangeToggleButton, deisotopingRangeMaxJFXTextField);
        addJFXToggleButtonListener(mmuToleranceToggleButton, mmuToleranceJFXTextField);

        // Binding JFXToggleButton to JFXButton & JFXTextField in settings-annotations pane.
        addJFXToggleButtonListener(adductAnnotationsJFXToggleButton, adductAnnotationsEditJFXButton);
        addJFXToggleButtonListener(neutralAnnotationsJFXToggleButton, neutralAnnotationsEditJFXButton);
        addJFXToggleButtonListener(isotopeAnnotationsJFXToggleButton, isotopeAnnotationsEditJFXButton);
        addJFXToggleButtonListener(adductAnnotationsJFXToggleButton, adductAnnotationsImportJFXButton);
        addJFXToggleButtonListener(neutralAnnotationsJFXToggleButton, neutralAnnotationsImportJFXButton);
        addJFXToggleButtonListener(isotopeAnnotationsJFXToggleButton, isotopeAnnotationsImportJFXButton);
        addJFXToggleButtonListener(adductAnnotationsJFXToggleButton, adductAnnotationsResetJFXButton);
        addJFXToggleButtonListener(neutralAnnotationsJFXToggleButton, neutralAnnotationsResetJFXButton);
        addJFXToggleButtonListener(isotopeAnnotationsJFXToggleButton, isotopeAnnotationsResetJFXButton);
        addJFXToggleButtonListener(multimerAnnotationsJFXToggleButton, multimerAnnotationsJFXTextField);
        addJFXToggleButtonListener(multichargeAnnotationsJFXToggleButton, multichargeAnnotationsJFXTextField);

        // Binding JFXToggleButton to JFXTextField in settings-analysis pane.
        addJFXToggleButtonListener(candidateIntensityJFXToggleButton, candidateIntensityPercentJFXTextField);
        addJFXToggleButtonListener(candidateIntensityJFXToggleButton, candidateIntensityCountsJFXTextField);
        addJFXToggleButtonListener(clusterConnectivityIndexJFXToggleButton, clusterConnectivityIndexJFXTextField);
        addJFXToggleButtonListener(clusterIntensityCoverageJFXToggleButton, clusterIntensityCoverageJFXTextField);
        addJFXToggleButtonListener(clusterCountCoverageJFXToggleButton, clusterCountCoverageJFXTextField);

        // Adding ToolTip to JFXButton in project-export pane.
        addToolTipToJFXButton(adductAnnotationsEditJFXButton, "Edit");
        addToolTipToJFXButton(neutralAnnotationsEditJFXButton, "Edit");
        addToolTipToJFXButton(isotopeAnnotationsEditJFXButton, "Edit");
        addToolTipToJFXButton(adductAnnotationsImportJFXButton, "Import");
        addToolTipToJFXButton(neutralAnnotationsImportJFXButton, "Import");
        addToolTipToJFXButton(isotopeAnnotationsImportJFXButton, "Import");
        addToolTipToJFXButton(adductAnnotationsResetJFXButton, "Reset");
        addToolTipToJFXButton(neutralAnnotationsResetJFXButton, "Reset");
        addToolTipToJFXButton(isotopeAnnotationsResetJFXButton, "Reset");

        inputJFXTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != oldValue && newValue != null) {

                File tempFile = new File(newValue);

                if (tempFile.isFile()) {
                    outputFileChooser.setInitialFileName(tempFile.getName());
                }
            }
        });
    }

    private MainFx mainFx;

    private Task task;

    private final ObjectProperty<File> inputFile;
    private final ObjectProperty<File> outputFile;

    private final ObservableSet<Annotation> activatedAdductsAnnotationSet;
    private final ObservableSet<Annotation> activatedNeutralsAnnotationSet;
    private final ObservableSet<Annotation> activatedIsotopesAnnotationSet;

    private final ObservableSet<Annotation> deactivatedAdductsAnnotationSet;
    private final ObservableSet<Annotation> deactivatedNeutralsAnnotationSet;
    private final ObservableSet<Annotation> deactivatedIsotopesAnnotationSet;

    private Path annotationsDirectoryPath;
    private Path candidatesDirectoryPath;
    private Path graphsDirectoryPath;

    private final FileChooser inputFileChooser;
    private final FileChooser outputFileChooser;
    private final FileChooser importFileChooser;

    private final DirectoryChooser inputDirectoryChooser;
    private final DirectoryChooser outputDirectoryChooser;

    public MainStageController() {
        inputFile = new SimpleObjectProperty();
        outputFile = new SimpleObjectProperty();

        activatedAdductsAnnotationSet = FXCollections.observableSet();
        activatedNeutralsAnnotationSet = FXCollections.observableSet();
        activatedIsotopesAnnotationSet = FXCollections.observableSet();

        activatedAdductsAnnotationSet.addAll(AnnotationSingleton.INSTANCE.getAdductDefaultAnnotationSet());
        activatedNeutralsAnnotationSet.addAll(AnnotationSingleton.INSTANCE.getNeutralDefaultAnnotationSet());
        activatedIsotopesAnnotationSet.addAll(AnnotationSingleton.INSTANCE.getIsotopeDefaultAnnotationSet());

        deactivatedAdductsAnnotationSet = FXCollections.observableSet();
        deactivatedNeutralsAnnotationSet = FXCollections.observableSet();
        deactivatedIsotopesAnnotationSet = FXCollections.observableSet();

        inputFileChooser = new FileChooser();
        inputFileChooser.setTitle("Choose Input File");
        inputFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Spectrum (*.mgf)", "*.mgf"));

        outputFileChooser = new FileChooser();
        outputFileChooser.setTitle("Choose Output File");
        outputFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Table (*.csv)", "*.csv"));

        importFileChooser = new FileChooser();
        importFileChooser.setTitle("Choose Annotation File");
        importFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Table (*.csv)", "*.csv"));

        inputDirectoryChooser = new DirectoryChooser();
        inputDirectoryChooser.setTitle("Choose Input Directory");

        outputDirectoryChooser = new DirectoryChooser();
        outputDirectoryChooser.setTitle("Choose Output Directory");
    }


    private boolean isAppRunnable() {

        if (!inputFile.get().exists()) {
            Platform.runLater(() -> showErrorDialog("File Not Found Error",
                    "An error has occurred while importing files. The <import file>\n" +
                            "could not be found or is corrupted."));
            return false;
        }

        if (intensityThresholdToggleButton.isSelected() && intensityPercentThresholdJFKTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <threshold>\nvalue in the <parameters> panel is not correctly formatted."));
            return false;
        }

        if (intensityThresholdToggleButton.isSelected() && intensityCountsThresholdJFKTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <threshold>\nvalue in the <parameters> panel is not correctly formatted."));
            return false;
        }

        if (mzRangeToggleButton.isSelected() && mzRangeMinJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <m/z Range>\nvalue in the <parameters> panel is not correctly formatted."));
            return false;
        }

        if (mzRangeToggleButton.isSelected() && mzRangeMaxJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <m/z Range>\nvalue in the <parameters> panel is not correctly formatted."));
            return false;
        }

        if (deisotopingRangeToggleButton.isSelected() && deisotopingRangeMinJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <deisotoping>\nvalue in the <parameters> panel is not correctly formatted."));
            return false;
        }

        if (deisotopingRangeToggleButton.isSelected() && deisotopingRangeMaxJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <deisotoping>\nvalue in the <parameters> panel is not correctly formatted."));
            return false;
        }

        if (mmuToleranceToggleButton.isSelected() && mmuToleranceJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <tolerance>\nvalue in the <parameters> panel is not correctly formatted."));
            return false;
        }

        if (candidateIntensityJFXToggleButton.isSelected() && candidateIntensityPercentJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <peak intensity>\nvalue in the <identification> panel is not correctly formatted."));
            return false;
        }

        if (candidateIntensityJFXToggleButton.isSelected() && candidateIntensityCountsJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <peak intensity>\nvalue in the <identification> panel is not correctly formatted."));
            return false;
        }

        if (clusterConnectivityIndexJFXToggleButton.isSelected() && clusterConnectivityIndexJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <peak connectivity>\nvalue in the <identification> panel is not correctly formatted."));
            return false;
        }

        if (clusterIntensityCoverageJFXToggleButton.isSelected() && clusterIntensityCoverageJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <intensity coverage>\nvalue in the <identification> panel is not correctly formatted."));
            return false;
        }

        if (clusterCountCoverageJFXToggleButton.isSelected() && clusterCountCoverageJFXTextField.getStyle() != "") {
            Platform.runLater(() -> showErrorDialog("Number Format Error",
                    "An error has occurred while reading settings." +
                            " The <peak connectivity>\nvalue in the <identification> panel is not correctly formatted."));
            return false;
        }

        return true;
    }

    private GraphGeneratorParams retrieveSettingsFromJFXTextfields() {

        GraphGeneratorParams graphGeneratorParams = new GraphGeneratorParams();

        if (intensityThresholdToggleButton.isSelected()) {
            graphGeneratorParams.setIntensityPercentThreshold(new Threshold(
                    Double.parseDouble(intensityPercentThresholdJFKTextField.getText()), Threshold.Limit.HIGHER));
            graphGeneratorParams.setIntensityCountsThreshold(new Threshold(
                    Double.parseDouble(intensityCountsThresholdJFKTextField.getText()), Threshold.Limit.HIGHER));
        } else {
            graphGeneratorParams.setIntensityPercentThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
            graphGeneratorParams.setIntensityCountsThreshold(new Threshold(0.0, Threshold.Limit.HIGHER));
        }

        if (mzRangeToggleButton.isSelected()) {
            graphGeneratorParams.setMzRange(Range.closed(Double.parseDouble(mzRangeMinJFXTextField.getText()),
                    Double.parseDouble(mzRangeMaxJFXTextField.getText())));
        } else {
            graphGeneratorParams.setMzRange(Range.atLeast(0.0));
        }

        if (deisotopingRangeToggleButton.isSelected()) {
            graphGeneratorParams.setDeisotopingRange(Range.closed(Double.parseDouble(deisotopingRangeMinJFXTextField.getText()),
                    Double.parseDouble(deisotopingRangeMaxJFXTextField.getText())));
        }

        if (mmuToleranceToggleButton.isSelected()) {
            graphGeneratorParams.setTolerance(new Tolerance(Double.parseDouble(mmuToleranceJFXTextField.getText()) / 1000));
        }

        if (adductAnnotationsJFXToggleButton.isSelected()) {
            graphGeneratorParams.getAnnotationTypeSet().add(Annotation.Type.ADDUCT);
            graphGeneratorParams.getAdductAnnotationSet().addAll(activatedAdductsAnnotationSet);
        }

        if (neutralAnnotationsJFXToggleButton.isSelected()) {
            graphGeneratorParams.getAnnotationTypeSet().add(Annotation.Type.NEUTRAL);
            graphGeneratorParams.getNeutralAnnotationSet().addAll(activatedNeutralsAnnotationSet);
        }

        if (isotopeAnnotationsJFXToggleButton.isSelected()) {
            graphGeneratorParams.getAnnotationTypeSet().add(Annotation.Type.ISOTOPE);
            graphGeneratorParams.getIsotopeAnnotationSet().addAll(activatedIsotopesAnnotationSet);
        }

        if (multimerAnnotationsJFXToggleButton.isSelected()) {
            graphGeneratorParams.getAnnotationTypeSet().add(Annotation.Type.MULTIMER);
            graphGeneratorParams.setMassMultiplier(new MzMultiplier(
                    Integer.parseInt(multimerAnnotationsJFXTextField.getText()), MzMultiplier.Type.MASS));
        }

        if (multichargeAnnotationsJFXToggleButton.isSelected()) {
            graphGeneratorParams.getAnnotationTypeSet().add(Annotation.Type.MULTICHARGE);
            graphGeneratorParams.setChargeMultiplier(new MzMultiplier(
                    Integer.parseInt(multichargeAnnotationsJFXTextField.getText()), MzMultiplier.Type.CHARGE));
        }

        if (candidateIntensityJFXToggleButton.isSelected()) {
            graphGeneratorParams.setCompoundPeakIntensityPercentThreshold(new Threshold(
                    Double.parseDouble(candidateIntensityPercentJFXTextField.getText()), Threshold.Limit.HIGHER));
            graphGeneratorParams.setCompoundPeakIntensityCountsThreshold(new Threshold(
                    Double.parseDouble(candidateIntensityCountsJFXTextField.getText()), Threshold.Limit.HIGHER));
        }

        if (clusterConnectivityIndexJFXToggleButton.isSelected()) {
            graphGeneratorParams.setClusterConnectivityIndexThreshold(new Threshold(
                    Double.parseDouble(clusterConnectivityIndexJFXTextField.getText()), Threshold.Limit.HIGHER
            ));
        }

        if (clusterIntensityCoverageJFXToggleButton.isSelected()) {
            graphGeneratorParams.setClusterIntensityCoverageThreshold(new Threshold(
                    Double.parseDouble(clusterIntensityCoverageJFXTextField.getText()), Threshold.Limit.HIGHER));
        }

        if (clusterCountCoverageJFXToggleButton.isSelected()) {
            graphGeneratorParams.setClusterCountCoverageThreshold(new Threshold(
                    Double.parseDouble(clusterCountCoverageJFXTextField.getText()), Threshold.Limit.HIGHER));
        }

        return graphGeneratorParams;
    }

    private File[] createInputFileArray() {

        if (inputFile.get().isDirectory()) {
            return inputFile.get().listFiles((directory, filename) -> filename.endsWith(".mgf"));
        } else {
            return new File[]{inputFile.get()};
        }
    }

    private File[] createOutputFileArray(File[] inputFiles) {

        File[] outputFiles = new File[inputFiles.length];

        if (outputFile.get().isDirectory()) {
            for (int i = 0; i < inputFiles.length; i++) {
                outputFiles[i] = Paths.get(outputFile.get().getPath(), inputFiles[i].getName()).toFile();
            }
        } else {
            outputFiles = new File[]{outputFile.get()};
        }
        return outputFiles;
    }

    private void createDirectoriesFromPath(Path path) {

        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException ioExceptionObj) {
                ioExceptionObj.printStackTrace();
            }
        }
    }

    private Path generateOutputDirectoryPath(File file, String... directories) {

        Path path;

        if (file.isDirectory()) {
            path = Paths.get(file.getPath());
        } else {
            path = Paths.get(file.getParent());
        }

        for (String directory : directories) {
            path = Paths.get(path.toString(), directory);
        }

        return path;
    }

    private void generateOutputDirectories() {

        String dir1 = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());

        if (annotationsExportToggleButton.isSelected()) {
            if (annotationsNewDirectoryJFXRadioButton.isSelected()) {
                annotationsDirectoryPath = generateOutputDirectoryPath(outputFile.get(), dir1, "annotations");
                createDirectoriesFromPath(annotationsDirectoryPath);
            } else {
                annotationsDirectoryPath = generateOutputDirectoryPath(outputFile.get(), dir1);
                createDirectoriesFromPath(annotationsDirectoryPath);
            }
        } else {
            annotationsDirectoryPath = null;
        }
        if (compoundsExportToggleButton.isSelected()) {
            if (candidatesNewDirectoryJFXRadioButton.isSelected()) {
                candidatesDirectoryPath = generateOutputDirectoryPath(outputFile.get(), dir1, "candidates");
                createDirectoriesFromPath(candidatesDirectoryPath);
            } else {
                candidatesDirectoryPath = generateOutputDirectoryPath(outputFile.get(), dir1);
                createDirectoriesFromPath(candidatesDirectoryPath);
            }
        } else {
            candidatesDirectoryPath = null;
        }
        if (clustersExportToggleButton.isSelected()) {
            if (graphsNewDirectoryJFXRadioButton.isSelected()) {
                graphsDirectoryPath = generateOutputDirectoryPath(outputFile.get(), dir1, "graphs");
                createDirectoriesFromPath(graphsDirectoryPath);
            } else {
                graphsDirectoryPath = generateOutputDirectoryPath(outputFile.get(), dir1);
                createDirectoriesFromPath(graphsDirectoryPath);
            }
        } else {
            graphsDirectoryPath = null;
        }
    }

    private File generateOutputFile(Path directoryPath, File file, String... items) {

        String fileName = file.getName().substring(0, file.getName().indexOf("."));

        for (String item : items) {
            fileName += item;
        }

        return new File(Paths.get(directoryPath.toString(), fileName).toString());
    }

    private Task<Void> createProjectExportTask() {

        Task task = new Task<Void>() {
            @Override
            protected Void call() throws JMzReaderException {

                final GraphGeneratorParams graphGeneratorParams = retrieveSettingsFromJFXTextfields();

                final File[] inputFiles = createInputFileArray();
                final File[] outputFiles = createOutputFileArray(inputFiles);

                generateOutputDirectories();

                List<SpectrumRecord> spectrumRecordList = new ArrayList<>();

                for (int fileIndex = 0; fileIndex < inputFiles.length; fileIndex++) {

                    if (isCancelled()) {
                        break;
                    }

                    JMzReader jMzReader = new MgfFile(inputFiles[fileIndex]);

                    int spectraCount = jMzReader.getSpectraCount();

                    for (int spectrumIndex = 1; spectrumIndex <= spectraCount; spectrumIndex++) {

                        updateMessage(inputFiles[fileIndex].getName());

                        if (isCancelled()) {
                            break;
                        }

                        Spectrum spectrum = jMzReader.getSpectrumByIndex(spectrumIndex);

                        SpectrumRecord spectrumRecord = new SpectrumRecord(inputFiles[fileIndex].getName(), spectrumIndex, spectrum);
                        SimpleDirectedWeightedGraph<Peak, AnnotatedWeightedEdge> jGraph = new SimpleDirectedWeightedGraph<>(AnnotatedWeightedEdge.class);

                        GraphVertexGenerator graphVertexGenerator = new GraphVertexGenerator(jGraph, graphGeneratorParams);
                        graphVertexGenerator.process(spectrum);

                        GraphEdgeGenerator graphEdgeGenerator = new GraphEdgeGenerator(jGraph, graphGeneratorParams);
                        graphEdgeGenerator.process();

                        if (isCancelled()) {
                            break;
                        }

                        if (annotationsExportToggleButton.isSelected()) {
                            GraphAnnotationsTableExporter graphAnnotationTableExport = new GraphAnnotationsTableExporter(jGraph, graphGeneratorParams);
                            graphAnnotationTableExport.export(generateOutputFile(annotationsDirectoryPath, outputFiles[fileIndex],
                                    "_spectrum", Integer.toString(spectrumIndex), "_annotations", ".csv"));
                        }

                        if (compoundsExportToggleButton.isSelected()) {
                            GraphCandidatesTableExporter graphCompoundTableExport = new GraphCandidatesTableExporter(jGraph, graphGeneratorParams);
                            graphCompoundTableExport.export(generateOutputFile(candidatesDirectoryPath, outputFiles[fileIndex],
                                    "_spectrum", Integer.toString(spectrumIndex), "_candidates", ".csv"), spectrumRecord);
                        }

                        if (clustersExportToggleButton.isSelected()) {
                            GraphClustersTableExporter graphClustersTableExporter = new GraphClustersTableExporter(jGraph, graphGeneratorParams);
                            graphClustersTableExporter.export(generateOutputFile(graphsDirectoryPath, outputFiles[fileIndex],
                                    "_spectrum", Integer.toString(spectrumIndex), "_graph", ".graphml"));
                        }

                        spectrumRecordList.add(spectrumRecord);

                        updateProgress(fileIndex + 1 / spectraCount, inputFiles.length);
                    }
                }

                if (compoundsExportToggleButton.isSelected() && spectrumRecordList.size() > 1) {
                    if (inputFiles.length > 1) {
                        SpectrumRecordExport spectrumRecordExport = new SpectrumRecordExport();
                        spectrumRecordExport.export(generateOutputFile(candidatesDirectoryPath, new File("summary.summary"), ".csv"), spectrumRecordList);
                    } else {
                        SpectrumRecordExport spectrumRecordExport = new SpectrumRecordExport();
                        spectrumRecordExport.export(generateOutputFile(candidatesDirectoryPath, outputFiles[0], "_summary", ".csv"), spectrumRecordList);
                    }
                }

                if (!isCancelled()) {
                    updateProgress(100.0, 100.0);
                }

                return null;
            }
        };

        task.setOnFailed(evt -> Platform.runLater(() -> showErrorDialog("Error",
                task.getException().toString())));

        task.setOnFailed(evt -> Platform.runLater(() -> task.getException().printStackTrace()));

        return task;
    }

    private Task<Void> createAnnotationImportTask(File file, Set<Annotation> annotationSet, Annotation.Type annotationType) {

        Task task = new Task<Void>() {
            @Override
            protected Void call() {

                List<Map> table = null;
                try {
                    table = CsvFileReader.read(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                updateMessage(file.getName());

                Set<Annotation> tempAnnotationSet = new HashSet<>();

                for (int rowIndex = 0; rowIndex < table.size(); rowIndex++) {

                    if (isCancelled()) {
                        break;
                    }

                    double mass = Double.parseDouble(table.get(rowIndex).get("mass").toString());

                    if (annotationType == Annotation.Type.NEUTRAL) {
                        tempAnnotationSet.add(new Annotation(annotationType,
                                new MzShift(table.get(rowIndex).get("name").toString(), -1 * Math.abs(mass))));
                    } else {
                        tempAnnotationSet.add(new Annotation(annotationType,
                                new MzShift(table.get(rowIndex).get("name").toString(), Math.abs(mass))));
                    }

                    updateProgress(rowIndex + 1, table.size());
                }

                if (!tempAnnotationSet.isEmpty()) {
                    annotationSet.clear();
                    annotationSet.addAll(tempAnnotationSet);
                }

                return null;
            }
        };

        task.setOnFailed(evt -> Platform.runLater(() -> showErrorDialog("Error",
                task.getException().toString())));

        return task;
    }


    public MainFx getMainFx() {
        return mainFx;
    }

    public void setMainFx(MainFx mainFx) {
        this.mainFx = mainFx;
    }

    private void showErrorDialog(String title, String content) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(MainFx.class.getClassLoader().getResource("org/expasy/mzadan/fx/fxml/errorDialog.fxml"));

        JFXDialog jfxDialog = null;

        try {
            jfxDialog = new JFXDialog(mainStackPane, fxmlLoader.load(), JFXDialog.DialogTransition.TOP);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ErrorDialogController controller = fxmlLoader.getController();
        controller.setDialog(jfxDialog);
        controller.setDialogTitle(title);
        controller.setDialogContent(content);

        jfxDialog.show();
    }

    private void showProgressDialog(Task task) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(MainFx.class.getClassLoader().getResource("org/expasy/mzadan/fx/fxml/progressDialog.fxml"));

        JFXDialog jfxDialog = null;

        try {
            jfxDialog = new JFXDialog(mainStackPane, fxmlLoader.load(), JFXDialog.DialogTransition.TOP);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ProgressDialogController controller = fxmlLoader.getController();
        controller.setDialog(jfxDialog);
        controller.setTask(task);

        jfxDialog.show();
    }

    private void showEditDialog(String title, ObservableSet<Annotation> activatedAnnotationSet, ObservableSet<Annotation> deactivatedAnnotationSet) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(MainFx.class.getClassLoader().getResource("org/expasy/mzadan/fx/fxml/editDialog.fxml"));

        JFXDialog jfxDialog = null;

        try {
            jfxDialog = new JFXDialog(mainStackPane, fxmlLoader.load(), JFXDialog.DialogTransition.TOP);
        } catch (IOException e) {
            e.printStackTrace();
        }

        EditDialogController controller = fxmlLoader.getController();
        controller.setDialog(jfxDialog);
        controller.setTitleLabel(title);
        controller.setActivatedAnnotationSet(activatedAnnotationSet);
        controller.setDeactivatedAnnotationSet(deactivatedAnnotationSet);

        jfxDialog.show();
    }

    private void editButtonAction(String title, ObservableSet<Annotation> activatedAnnotationSet, ObservableSet<Annotation> deactivatedAnnotationSet) {
        showEditDialog(title, activatedAnnotationSet, deactivatedAnnotationSet);
    }

    private void importButtonAction(Set<Annotation> annotationSet, Annotation.Type annotationType) {

        File file = importFileChooser.showOpenDialog(mainFx.getPrimaryStage());

        if (file != null) {
            task = createAnnotationImportTask(file, annotationSet, annotationType);

            Platform.runLater(() -> showProgressDialog(task));

            Thread thread = new Thread(task);
            thread.setDaemon(true);
            thread.start();
        }
    }

    private void resetButtonAction(Set<Annotation> activatedAnnotationSet, Set<Annotation> deactivatedAnnotationSet, Set<Annotation> expectedAnnotationSet) {
        activatedAnnotationSet.clear();
        deactivatedAnnotationSet.clear();

        activatedAnnotationSet.addAll(expectedAnnotationSet);
    }

    @FXML
    public void handleExportButtonOnAction() {
        if (isAppRunnable()) {
            task = createProjectExportTask();

            Platform.runLater(() -> showProgressDialog(task));

            Thread thread = new Thread(task);
            thread.setDaemon(true);
            thread.start();
        }
    }

    @FXML
    public void handleInputButtonOnAction() {
        if (inputProjectJFXCombobox.getValue().getText().equals("File")) {
            File file = inputFileChooser.showOpenDialog(mainFx.getPrimaryStage());
            if (file != null) {
                inputFile.set(file);
            }
        } else if (inputProjectJFXCombobox.getValue().getText().equals("Directory")) {
            File file = inputDirectoryChooser.showDialog(mainFx.getPrimaryStage());
            if (file != null) {
                inputFile.set(file);
            }
        }
    }

    @FXML
    private void handleOutputButtonOnAction() {
        if (inputProjectJFXCombobox.getValue().getText().equals("File")) {
            File file = outputFileChooser.showSaveDialog(mainFx.getPrimaryStage());
            if (file != null) {
                outputFile.set(file);
            }
        } else if (inputProjectJFXCombobox.getValue().getText().equals("Directory")) {
            File file = outputDirectoryChooser.showDialog(mainFx.getPrimaryStage());
            if (file != null) {
                outputFile.set(file);
            }
        }
    }

    @FXML
    public void handleAdductsEditButtonOnAction() {
        editButtonAction("Adducts", activatedAdductsAnnotationSet, deactivatedAdductsAnnotationSet);
    }

    @FXML
    public void handleNeutralsEditButtonOnAction() {
        editButtonAction("Neutrals", activatedNeutralsAnnotationSet, deactivatedNeutralsAnnotationSet);
    }

    @FXML
    public void handleIsotopesEditButtonOnAction() {
        editButtonAction("Isotopes", activatedIsotopesAnnotationSet, deactivatedIsotopesAnnotationSet);
    }

    @FXML
    public void handleAdductsImportButtonOnAction() {
        importButtonAction(activatedAdductsAnnotationSet, Annotation.Type.ADDUCT);
    }

    @FXML
    public void handleNeutralsImportButtonOnAction() {
        importButtonAction(activatedNeutralsAnnotationSet, Annotation.Type.NEUTRAL);
    }

    @FXML
    public void handleIsotopesImportButtonOnAction() {
        importButtonAction(activatedIsotopesAnnotationSet, Annotation.Type.ISOTOPE);
    }

    @FXML
    public void handleAdductsResetButtonOnAction() {
        resetButtonAction(activatedAdductsAnnotationSet, deactivatedAdductsAnnotationSet, AnnotationSingleton.INSTANCE.getAdductDefaultAnnotationSet());
    }

    @FXML
    public void handleNeutralsResetButtonOnAction() {
        resetButtonAction(activatedNeutralsAnnotationSet, deactivatedNeutralsAnnotationSet, AnnotationSingleton.INSTANCE.getNeutralDefaultAnnotationSet());
    }

    @FXML
    public void handleIsotopesResettButtonOnAction() {
        resetButtonAction(activatedIsotopesAnnotationSet, deactivatedIsotopesAnnotationSet, AnnotationSingleton.INSTANCE.getIsotopeDefaultAnnotationSet());
    }

    @FXML
    public void handleHomeButtonOnAction() {
        homeAnchorPane.toFront();
    }

    @FXML
    public void handleProjectButtonOnAction() {
        projectAnchorPane.toFront();
    }

    @FXML
    public void handleVisualizationProjectButtonOnAction() {
        projectVisualizationAnchorPane.toFront();
    }

    @FXML
    public void handleExportProjectButtonOnAction() {
        projectExportAnchorPane.toFront();
    }

    @FXML
    public void handleSettingsButtonOnAction() {
        settingsAnchorPane.toFront();
    }

    @FXML
    public void handleParametersSettingsButtonOnAction() {
        parametersSettingsAnchorPane.toFront();
    }

    @FXML
    public void handleAnnotationsSettingsButtonOnAction() {
        annotationsSettingsAnchorPane.toFront();
    }

    @FXML
    public void handleIdentificationSettingsButtonOnAction() {
        identificationParametersAnchorPane.toFront();
    }

    @FXML
    public void handlHelpButtonOnAction() {
    }

    @FXML
    public void handleQuitButtonOnAction() {
        task.cancel();
        mainFx.getPrimaryStage().close();
    }
}
