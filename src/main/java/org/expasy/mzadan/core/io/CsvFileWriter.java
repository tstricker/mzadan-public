package org.expasy.mzadan.core.io;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Thomas on 06.04.17.
 */
public class CsvFileWriter {

    private CsvFileWriter() {
    }

    public static void writeLines(File file, List<List<String>> content) {

        FileWriter fileWriter = null;
        CSVPrinter csvFilePrinter = null;

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");

        try {

            fileWriter = new FileWriter(file);
            csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

            for (List line : content) {
                csvFilePrinter.printRecord(line);
            }

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter!");
            e.printStackTrace();

        } finally {

            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();

            } catch (IOException e) {
                System.out.println("Error while flushing or closing fileWriter or csvPrinter!");
                e.printStackTrace();
            }
        }
    }
}

