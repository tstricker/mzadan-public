package org.expasy.mzadan.core.ms;


import com.google.common.collect.Range;
import org.expasy.mzadan.core.graph.model.GraphGeneratorParams;
import org.expasy.mzadan.core.graph.utils.VertexUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;

import java.io.File;
import java.util.List;

/**
 * Created by Thomas on 23.07.18.
 */

public class SpectrumFilteringTest {
    private static List<Peak> defaultPeakList0;
    private static List<Peak> defaultPeakList1;

    @BeforeClass
    public static void setUpClass() throws JMzReaderException {
        File file0 = new File(SpectrumFilteringTest.class.getClassLoader().getResource("VertexGeneratorTestSpectrum.mgf").getFile());
        File file1 = new File(SpectrumFilteringTest.class.getClassLoader().getResource("EdgeGeneratorTestSpectrum.mgf").getFile());

        JMzReader jMzReader0 = new MgfFile(file0);
        JMzReader jMzReader2 = new MgfFile(file1);

        Spectrum spectrum1 = jMzReader0.getSpectrumByIndex(1);
        Spectrum spectrum2 = jMzReader2.getSpectrumByIndex(1);

        defaultPeakList0 = VertexUtils.generateSortedPeakList(spectrum1, new Threshold(0.0, Threshold.Limit.HIGHER));
        defaultPeakList1 = VertexUtils.generateSortedPeakList(spectrum2, new Threshold(0.0, Threshold.Limit.HIGHER));
    }

    @Test
    public void SpectrumFilteringRelativeIntensityTest() {

        //Testing the conversion and filtering of peaks higher or equal to 0.0% of relative intensity.
        SpectrumFiltering spectrumFiltering0 = new SpectrumFiltering(defaultPeakList0,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList0 = spectrumFiltering0.process();

        Assert.assertEquals(26, peakList0.size());
        Assert.assertEquals(14988.14033, peakList0.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(54595, peakList0.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 0.001);

        //Testing the conversion and filtering of peaks higher or equal to 25.0% of relative intensity.
        SpectrumFiltering spectrumFiltering1 = new SpectrumFiltering(defaultPeakList0,
                new Threshold(25.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList1 = spectrumFiltering1.process();

        Assert.assertEquals(9, peakList1.size());
        Assert.assertEquals(4975.97648, peakList1.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(50000.0, peakList1.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks higher or equal to 50.0% of relative intensity.
        SpectrumFiltering spectrumFiltering2 = new SpectrumFiltering(defaultPeakList0,
                new Threshold(50.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList2 = spectrumFiltering2.process();

        Assert.assertEquals(5, peakList2.size());
        Assert.assertEquals(3076.96439, peakList2.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(40000, peakList2.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks higher or equal to 75.0% of relative intensity.
        SpectrumFiltering spectrumFiltering3 = new SpectrumFiltering(defaultPeakList0,
                new Threshold(75.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList3 = spectrumFiltering3.process();

        Assert.assertEquals(5, peakList3.size());
        Assert.assertEquals(3076.96439, peakList3.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(40000, peakList3.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks higher or equal to 100% of relative intensity.
        SpectrumFiltering spectrumFiltering4 = new SpectrumFiltering(defaultPeakList0,
                new Threshold(100.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList4 = spectrumFiltering4.process();

        Assert.assertEquals(1, peakList4.size());
        Assert.assertEquals(500, peakList4.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(10000, peakList4.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);
    }

    @Test
    public void SpectrumFilteringIntensityTest() {

        //Testing the conversion and filtering of peaks higher or equal to 0 Counts intensity.
        SpectrumFiltering spectrumFiltering = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList0 = spectrumFiltering.process();

        Assert.assertEquals(43, peakList0.size());
        Assert.assertEquals(20525.87986, peakList0.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(80305, peakList0.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks higher or equal to 100.0 Counts intensity.
        SpectrumFiltering spectrumFiltering1 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(100.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList1 = spectrumFiltering1.process();

        Assert.assertEquals(33, peakList1.size());
        Assert.assertEquals(15921.86306, peakList1.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(80250, peakList1.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks higher or equal to 1000.0 Counts intensity.
        SpectrumFiltering spectrumFiltering2 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(1000.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList2 = spectrumFiltering2.process();

        Assert.assertEquals(16, peakList2.size());
        Assert.assertEquals(7550.91473, peakList2.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(74500, peakList2.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks higher or equal to 5000.0 Counts intensity.
        SpectrumFiltering spectrumFiltering3 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(5000.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList3 = spectrumFiltering3.process();

        Assert.assertEquals(11, peakList3.size());
        Assert.assertEquals(5266.89648, peakList3.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(65000, peakList3.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks higher or equal to 10000.0 Counts intensity.
        SpectrumFiltering spectrumFiltering4 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(10000.0, Threshold.Limit.HIGHER),
                Range.atLeast(0.0));

        List<Peak> peakList4 = spectrumFiltering4.process();

        Assert.assertEquals(2, peakList4.size());
        Assert.assertEquals(750, peakList4.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(20000, peakList4.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);
    }

    @Test
    public void SpectrumFilteringMzRangeTest() {

        //Testing the conversion and filtering of peaks with mz ranging from 0.0 to 1500.0 m/z.
        SpectrumFiltering spectrumFiltering = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.closed(0.0, 1500.0));

        List<Peak> peakList0 = spectrumFiltering.process();

        Assert.assertEquals(43, peakList0.size());
        Assert.assertEquals(20525.87986, peakList0.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(80305, peakList0.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks with mz ranging from 250.0 to 1500.0 m/z.
        SpectrumFiltering spectrumFiltering1 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.closed(250.0, 1500.0));

        List<Peak> peakList1 = spectrumFiltering1.process();

        Assert.assertEquals(41, peakList1.size());
        Assert.assertEquals(20060.89762, peakList1.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(74805, peakList1.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks with mz ranging from 250.0 to 1000.0 m/z.
        SpectrumFiltering spectrumFiltering2 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.closed(250.0, 1000.0));

        List<Peak> peakList2 = spectrumFiltering2.process();

        Assert.assertEquals(41, peakList2.size());
        Assert.assertEquals(20060.89762, peakList2.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(74805.0, peakList2.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks with mz ranging from 500.0 to 1000.0 m/z.
        SpectrumFiltering spectrumFiltering3 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.closed(500.0, 1000.0));

        List<Peak> peakList3 = spectrumFiltering3.process();

        Assert.assertEquals(20, peakList3.size());
        Assert.assertEquals(11516.97846, peakList3.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(38722.0, peakList3.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);

        //Testing the conversion and filtering of peaks with mz ranging from 500.0 to 750.0 m/z.
        SpectrumFiltering spectrumFiltering4 = new SpectrumFiltering(defaultPeakList1,
                new Threshold(0.0, Threshold.Limit.HIGHER),
                new Threshold(0.0, Threshold.Limit.HIGHER),
                Range.closed(500.0, 750.0));

        List<Peak> peakList4 = spectrumFiltering4.process();

        Assert.assertEquals(18, peakList4.size());
        Assert.assertEquals(9517.98966, peakList4.stream().mapToDouble(peak -> peak.getMz()).sum(), 0.001);
        Assert.assertEquals(33222, peakList4.stream().mapToDouble(peak -> peak.getIntensity()).sum(), 1);
    }
}
