package org.expasy.mzadan.fx;

import com.jfoenix.controls.JFXDialog;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Thomas on 30.08.18.
 */
public class ErrorDialogController implements Initializable {
    private JFXDialog jfxDialog;

    @FXML
    private Label titleLabel;
    @FXML
    private Label contentLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setDialog(JFXDialog jfxDialog) {
        this.jfxDialog = jfxDialog;
    }

    public void setDialogTitle(String title){
        titleLabel.setText(title);
    }

    public void setDialogContent(String content){
        contentLabel.setText(content);
    }

    @FXML
    public void handleOkayButtonOnAction() {
        jfxDialog.close();
    }
}
